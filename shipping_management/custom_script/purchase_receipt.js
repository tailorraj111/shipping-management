frappe.ui.form.on('Purchase Receipt', {
    refresh(frm) {
		// your code here
	},
    department: function(frm){
        for(var i in frm.doc.items){
            frm.doc.items[i].department = frm.doc.department
        }
        frm.refresh_field("items")
    },
    division: function(frm){
        for(var i in frm.doc.items){
            frm.doc.items[i].division = frm.doc.division
        }
        frm.refresh_field("items")
    },
})

frappe.ui.form.on('Purchase Receipt Item', {
	refresh(frm) {
		// your code here
	},
    add_serial_number(frm,cdt,cdn){
        console.log("here");
        var row = locals[cdt][cdn];
        if(row.initial_serial_number && row.serial_quantity){
            var init_sn = row.initial_serial_number.substring(0, 4);
            var in_sn = parseInt(row.initial_serial_number.substring(4))
            var sn_str = []
            
            for(var i = 0; i < row.serial_quantity ; i++){
                var digit = in_sn + i;
                var len = row.initial_serial_number.substring(4).length;
                var len_digit = digit.toString().length;
                var temp_str = init_sn
                while(len > len_digit){
                    temp_str += "0"
                    len_digit += 1;
                }
                
                sn_str.push(temp_str + digit)
                 console.log(temp_str + digit)
            }
            if(row.serial_no){
                row.serial_no = row.serial_no + "\n" + sn_str.join("\n")
            }
            else{
                row.serial_no = sn_str.join("\n")
            }
            
            frm.refresh_field("items")
        }
    },
    clear_serial_number(frm,cdt,cdn){
        var row = locals[cdt][cdn];
        row.serial_no = ""
        frm.refresh_field("items")
        frm.dirty()
    }
})