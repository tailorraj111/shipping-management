frappe.ui.form.on('Payroll Entry', {
    refresh:function(frm){
        // console.log("here")
        if (frm.doc.docstatus == 0) {
            if(!frm.is_new()) {
                cur_frm.add_custom_button(__('Get Leave Encashment'), function() {
                    frappe.call({
                        "method":"shipping_management.shipping_management.payroll_entry.get_leave_amounts",
                        "args":{
                            "payroll_doc":frm.doc,
                        },
                        
                    })
                });
            }
            
        }
        
        // item_code is a field in grid whose fieldname is items.. refer Sales Order
       
    },
    start_date:function(frm){
        if(frm.doc.start_date){
            
            var date_string = frm.doc.start_date.toString()
            frm.set_value("custome_month",get_month(date_string.split("-")[1]))
            frm.refresh_field("custome_month")
            console.log(date_string.split("-")[1])
            console.log(get_month(date_string.split("-")[1]))
        }
        
    }
})
function get_month(month_num) {
    var month_array = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    index = parseInt(month_num) - 1
    return month_array[index]
    
}