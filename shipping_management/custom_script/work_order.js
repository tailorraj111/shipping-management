frappe.ui.form.on("Work Order", {
    refresh(frm){
        if (frm.doc.docstatus === 1) {
			if (frm.doc.status == 'Material Issued') {
				frm.add_custom_button(__('Start'), function() {
					frm.set_value("status","In Process")
					frm.refresh_field("In Process")
					frm.save('Update');
				}, __("Status"));
			}
        }
		if(frm.doc.actual_start_date && frm.doc.actual_end_date){
			var prod_days = frappe.datetime.get_day_diff( frm.doc.actual_end_date , frm.doc.actual_start_date )
			frm.set_value("production_days",prod_days)
			frm.refresh_field("production_days")
		}
    },
	validate(frm){
		if(frm.doc.actual_start_date && frm.doc.actual_end_date){
			var prod_days = frappe.datetime.get_day_diff( frm.doc.actual_end_date , frm.doc.actual_start_date )
			frm.set_value("production_days",prod_days)
			frm.refresh_field("production_days")
		}
	},
	on_update(frm){
		
	}
})