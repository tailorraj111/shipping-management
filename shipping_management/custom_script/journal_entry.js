frappe.ui.form.on('Journal Entry', {
    validate: function(frm) {
        
        frm.doc.accounts.forEach(function(row) {
            if (frm.doc.custom_cost_center) {
                row.cost_center = frm.doc.custom_cost_center;
            }
        });
        frm.refresh_field('accounts');
    }
});
