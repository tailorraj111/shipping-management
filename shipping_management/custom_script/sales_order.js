frappe.ui.form.on('Sales Order', {
    
    actual_payment_date:function(frm){
        if(frm.doc.actual_payment_date && frm.doc.commitment_date){
            frm.set_value("delay_days", frappe.datetime.get_day_diff( frm.doc.actual_payment_date, frm.doc.commitment_date ));
            frm.refresh_field("delay_days")
        }
      
    },
    
    make_payment_c:function(frm){
            if(!frm.doc.payment_entry){
                frappe.confirm('Are you sure you want make payment?',
                    () => {
                        frappe.call({
                            "method":"shipping_management.shipping_management.sales_order.make_payment_entry",
                            "args":{
                                "self":frm.doc
                            },
                            "callback":function(res){
                                console.log(res)
                                frm.set_value("payment_entry",res.message.name)
                                frm.refresh_field("payment_entry")
                            }
                        })
                    }, () => {
                       
                    })
                
            }else{
                frappe.msgprint("Payment Entry Made Already!")
            }
            
        
    },
    refresh_outstanding:function(frm){
        frappe.call({
            "method":"shipping_management.shipping_management.sales_order.refresh_account_data",
            "args":{
                "self":frm.doc
            },
            "callback":function(res){
                console.log(res)
                frm.set_value("credit_limit",res.message.credit_limit)
                frm.set_value("current_outstanding",res.message.current_outstanding)
                frm.set_value("outstanding_limit",res.message.outstanding_limit)
                frm.set_value("overdue_amount",res.message.overdue_amount)
                frm.set_value("approved_sales_order",res.message.approved_sales_order)
                frm.set_value("current_ledger_balance",res.message.current_ledger_balance)
                frm.refresh_field("credit_limit","current_outstanding","outstanding_limit","approved_sales_order","current_ledger_balance")
            }
        })
    },
    onload:function(frm){
       
        if(frm.is_new()){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_sales_person",
                "args":{
                    "user":frappe.session.user
                },
                callback:function(res){
                    console.log(res)
                    if(res.message){
                        frm.set_value("sales_team",[])
                        frm.refresh_field("sales_team")
                        var newrow = frappe.model.add_child(frm.doc, "Sales Team", "sales_team");
                        newrow.sales_person = res.message;
                        newrow.allocated_percentage = 100;
                        cur_frm.script_manager.trigger("sales_person", newrow.doctype, newrow.name);
                        frm.refresh_field("sales_team");
    
                    }
                }
    
            })
        }
        
    },
    setup: function(frm) {
		frm.set_query("shipping_method", function() {

			return {
				filters: {
					"item_group": "Shipping Item"
				}
			}
		});
    },
    validate:function(frm){
        frappe.call({
            "method":"shipping_management.shipping_management.sales_order.get_customer_outstanding",
            "args":{
                "customer":frm.doc.customer,
                "company":frm.doc.company
            },
            "callback":function(res){
                console.log(res)
                if(res.message[0] > 0 ){
                    frappe.msgprint("Customer has <b>"+parseFloat(res.message[0]).toFixed(2)+"</b> Outstanding amount!!")
                }
                
            }
        })

        frm.set_value("sales_person",frm.doc.sales_team[0].sales_person)
	    frm.refresh_field("sales_person")
        frappe.db.get_value("Customer",frm.doc.customer,"accounting_issue").then((res)=>{
           console.log(res.message.accounting_issue)
           if(res.message.accounting_issue){
               frappe.msgprint("Customer is having Accounting Issue. Please contact Accounts Department to resolve the issue. You can make Sales Order but Invoice is blocked.")
           }
           
        })
        
     
      
   },
    refresh :async function(frm, cdt, cdn) {
        if(frm.doc.docstatus == 1){
            frm.add_custom_button(__("Generate Picklist"), function(){
                frappe.call({
                    "method":"shipping_management.shipping_management.sales_order.generate_picklist",
                    "args":{
                        "doc":frm.doc
                    },
                    "callback":function(res){

                    }
                })
              });
        }
       
        
        
        cur_frm.add_custom_button(__('OEM Form'), function() {
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.oem_form",
                "args":{
                    "order_doc":frm.doc
                },
                "callback":function(res){
                    if(res.message){
                        frappe.set_route('app/oem-form/' + res.message)
                    }
                    else{
                        frappe.new_doc("OEM Form")
                    }     
                }
            })
        });
                
                
            

        if(frm.doc.status == "Closed" && (frm.doc.per_delivered != 100 || frm.doc.per_billed != 100)){
            console.log("Changed Delivered and Billed Values")
            frappe.db.set_value("Sales Order",frm.doc.name,"per_delivered",100)
            frappe.db.set_value("Sales Order",frm.doc.name,"per_billed",100)
        }

        cur_frm.add_custom_button(__('Check Stock Details'), function() {
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.check_stock",
                "args":{
                    "order_doc":frm.doc,
                },
                
            })
        });
  
      
        },
  
    department: function(frm){
        for(var i in frm.doc.taxes){
            frm.doc.items[i].department = frm.doc.department
        }
        frm.refresh_field("taxes")
    },
    division: function(frm){
        for(var i in frm.doc.taxes){
            frm.doc.items[i].division = frm.doc.division
        }
        frm.refresh_field("taxes")
    },    
    
    shipping_method:async function(frm){

        if(frm.doc.shipping_method)
        {    
          
            if(!frm.doc.shipping_rule_custom){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
                frappe.throw("Please Select Shipping Rule First!")
            }
    
            if (!frm.doc.customer){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
            }

            if (!frm.doc.total_net_weight){
                frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");            
            }
        
        let shipping_amount = await calculate_shipping_amount(frm.doc)
        var found = 0
        // update Shipping Item If already Present
        
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                frm.doc.items[i]['item_code'] = frm.doc.shipping_method
                frm.doc.items[i]['qty'] = 1
                frm.doc.items[i]['rate'] = shipping_amount.message
                found = 1
                refresh_field("items");
            }
        }       


        // If not Shipping Item then add it.
        if (frm.doc.shipping_method && !found){
            console.log("add item")
            frappe.call({
                method:"frappe.client.get",
                args:{
                    "doctype":"Item",
                    "name":frm.doc.shipping_method
                },
                callback:function(res){

                    console.log(res.message)
                    var newrow = frappe.model.add_child(frm.doc, "Sales Order Item", "items");
                    newrow.item_code = res.message['name'];
                    newrow.item_name = res.message['item_name'];
                    newrow.item_group = res.message['item_group'];
                    newrow.description = res.message['description'];
                    newrow.stock_uom = res.message['stock_uom'];
                    newrow.uom = res.message['stock_uom'];
                    newrow.income_account = res.message['item_defaults'][0]['income_account'];
                    newrow.rate = shipping_amount.message
                    newrow.qty = 1
                    cur_frm.script_manager.trigger("rate", newrow.doctype, newrow.name);       
                }
            })

        }
    }

        // If shipping Method Field is Empty then remove it from table
        if (!frm.doc.shipping_method){
            for(var i = 0; i < frm.doc.items.length; i++){
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
        } 
        var shipping = 0
        for(var i = 0; i < frm.doc.items.length; i++){         
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                shipping = shipping + flt(frm.doc.items[i]['amount'])
            }
        }
        frm.set_value("shipping_charges",shipping)
        frm.set_value("item_total",flt(frm.doc.total - shipping))
        refresh_field("shipping_charges","item_total");
        
        

    },
    custom_discount_percentage:function(frm){
        if(frm.doc.item_total){
            frm.set_value("discount_amount",flt(frm.doc.item_total*frm.doc.custom_discount_percentage)/100)
        }else{
            frm.set_value("discount_amount",flt(frm.doc.total*frm.doc.custom_discount_percentage)/100)
        }
        refresh_field("discount_amount")
        // console.log(frm.doc.item_total)
    },
    custom_discount_amount:function(frm){
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] != "Shipping Item"){
                var distributed_amount = (frm.doc.custom_discount_amount * frm.doc.items[i]['net_amount'])/frm.doc.item_total
                frappe.model.set_value(frm.doc.items[i].doctype, frm.doc.items[i].name , "net_amount", frm.doc.items[i]['net_amount'] - distributed_amount);
                refresh_field("items")	
            }
        }

        
    },
})

cur_frm.cscript.rate = function(doc,cdt,cdn){
    update_item_total(doc,cdt,cdn)
    }

function update_item_total(doc,cdt,cdn){
    var shipping = 0
    for(var i = 0; i < doc.items.length; i++){
       
        if(doc.items[i]['item_group'] == "Shipping Item"){
            shipping = shipping + flt(doc.items[i]['amount'])
            
        }
    }
   
    cur_frm.set_value("shipping_charges",shipping)
    cur_frm.set_value("item_total",flt(doc.total - shipping))
    refresh_field("shipping_charges","item_total");
}

frappe.ui.form.on('Sales Invoice Item', {
    items_add:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            for(var i = 0; i < frm.doc.items.length; i++){
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
       
    },
    items_remove:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
        refresh_field("shipping_method");
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                frm.doc.items.pop(frm.doc.items[i])
                refresh_field("items");
            }
        }
        
    }
})

async function calculate_shipping_amount(current_doc){
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                
                method:'shipping_management.shipping_management.quotation.apply_shipping_rule_custom',
                args: {
                    curr_doc: current_doc,
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });

}



