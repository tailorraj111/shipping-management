frappe.ui.form.on('Quotation', {
    onload:function(frm){
        if(frm.is_new()){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_sales_person",
                "args":{
                    "user":frappe.session.user
                },
                callback:function(res){
                    
                    if(res.message){
                        frm.set_value("sales_person",res.message)
                        frm.refresh_field("sales_person");
    
                    }
                }
    
            })
        }
    },
    setup: function(frm) {
		frm.set_query("shipping_method", function() {

			return {
				filters: {
					"item_group": "Shipping Item"
				}
			}
		});
    },
    validate:function(frm){
      
        if(frm.doc.quotation_to == "Customer"){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_customer_outstanding",
                "args":{
                    "customer":frm.doc.party_name,
                    "company":frm.doc.company
                },
                "callback":function(res){
                    
                    if(res.message > 0){
                        frappe.confirm(
                            'Customer has Outstanding of '+parseFloat(res.message[0]).toFixed(2)+'. Do you want to Continue?',
                            function(){
                                window.close();
                            },
                            function(){
                                frm.set_value("total_with_outstanding_c",frm.doc.total + parseFloat(res.message))
                                frm.refresh_field("total_with_outstanding_c")
                            }
                        )
                        
                    }
                }
            })

        }
       
    },
    refresh : async function(frm, cdt, cdn) {
        
        let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.party_name,"customer_loyalty");
        var loyalty_type = loyalty_type_res.message.customer_loyalty;
        let btn = document.createElement('span');
        console.log(loyalty_type)
    if(loyalty_type == "New"){
        {
            btn.innerText = 'New';
            btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
        }
    } else if(loyalty_type == "Silver") {
        {
            btn.innerText = 'Silver';
            btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
        }
    } else if(loyalty_type == "Gold") {
        {
            btn.innerText = 'Gold';
            btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
        }

    } else if(loyalty_type == "Platinum")
        {
            btn.innerText = 'Platinum';
            btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
        }


            var tool_bar = $("#page-Quotation > .page-head > .container > .page-head-content > .page-title")
            $( "#loayalty_indicator" ).remove();
            tool_bar.append(btn)
       
        frm.set_query("item_code", "items", function(doc) {
            if(doc.brand){
                return { "filters" : [['item_group','!=','Shipping Item'],['is_sales_item','=',1],['brand','=',doc.brand]]}
            }else{
                return { "filters" : [['item_group','!=','Shipping Item'],['is_sales_item','=',1]]}
            }
 
        }); 
        
        },
    


    // -----------------------------------shipping customization-----------------------------------
    shipping_method:async function(frm){

        if(frm.doc.shipping_method)
        {    
          
            if(!frm.doc.shipping_rule_custom){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
                frappe.throw("Please Select Shipping Rule First!")
            }
    
            if (!frm.doc.party_name){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
            }

            if (!frm.doc.total_net_weight){
                frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");            
            }
        
        let shipping_amount = await calculate_shipping_amount(frm.doc)
        var found = 0
        // update Shipping Item If already Present
        
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                frm.doc.items[i]['item_code'] = frm.doc.shipping_method
                frm.doc.items[i]['qty'] = 1
                frm.doc.items[i]['rate'] = shipping_amount.message
                found = 1
                refresh_field("items");
            }
        }       


        // If not Shipping Item then add it.
        if (frm.doc.shipping_method && !found){
            console.log("add item")
            frappe.call({
                method:"frappe.client.get",
                args:{
                    "doctype":"Item",
                    "name":frm.doc.shipping_method
                },
                callback:function(res){

                    console.log(res.message)
                    var newrow = frappe.model.add_child(frm.doc, "Quotation Item", "items");
                    newrow.item_code = res.message['name'];
                    newrow.item_name = res.message['item_name'];
                    newrow.item_group = res.message['item_group'];
                    newrow.description = res.message['description'];
                    newrow.stock_uom = res.message['stock_uom'];
                    newrow.uom = res.message['stock_uom'];
                    newrow.income_account = res.message['item_defaults'][0]['income_account'];
                    newrow.rate = shipping_amount.message
                    newrow.qty = 1
                    cur_frm.script_manager.trigger("rate", newrow.doctype, newrow.name);       
                }
            })

        }
    }

        // If shipping Method Field is Empty then remove it from table
        if (!frm.doc.shipping_method){
            for(var i = 0; i < frm.doc.items.length; i++){
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
        } 
        var shipping = 0
        for(var i = 0; i < frm.doc.items.length; i++){         
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                shipping = shipping + flt(frm.doc.items[i]['amount'])
            }
        }
        frm.set_value("shipping_charges",shipping)
        frm.set_value("item_total",flt(frm.doc.total - shipping))
        refresh_field("shipping_charges","item_total");
        
        

    },
    custom_discount_percentage:function(frm){
        if(frm.doc.item_total){
            frm.set_value("discount_amount",flt(frm.doc.item_total*frm.doc.custom_discount_percentage)/100)
        }else{
            frm.set_value("discount_amount",flt(frm.doc.total*frm.doc.custom_discount_percentage)/100)
        }
        refresh_field("discount_amount")
        // console.log(frm.doc.item_total)
    },
    custom_discount_amount:function(frm){
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] != "Shipping Item"){
                var distributed_amount = (frm.doc.custom_discount_amount * frm.doc.items[i]['net_amount'])/frm.doc.item_total
                frappe.model.set_value(frm.doc.items[i].doctype, frm.doc.items[i].name , "net_amount", frm.doc.items[i]['net_amount'] - distributed_amount);
                refresh_field("items")	
            }
        }

        
    },
})

cur_frm.cscript.rate = function(doc,cdt,cdn){
    update_item_total(doc,cdt,cdn)
    }

function update_item_total(doc,cdt,cdn){
    var shipping = 0
    for(var i = 0; i < doc.items.length; i++){
       
        if(doc.items[i]['item_group'] == "Shipping Item"){
            shipping = shipping + flt(doc.items[i]['amount'])
            
        }
    }
   
    cur_frm.set_value("shipping_charges",shipping)
    cur_frm.set_value("item_total",flt(doc.total - shipping))
    refresh_field("shipping_charges","item_total");
}

frappe.ui.form.on('Quotation Item', {
    items_add:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            for(var i = 0; i < frm.doc.items.length; i++){
                // console.log(frm.doc.items[i]['item_group'])
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    // console.log("inif   ")
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
       
    },
    items_remove:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
        refresh_field("shipping_method");
        for(var i = 0; i < frm.doc.items.length; i++){
            // console.log(frm.doc.items[i]['item_group'])
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                // console.log("inif   ")
                frm.doc.items.pop(frm.doc.items[i])
                refresh_field("items");
            }
        }
        
    }
})

async function calculate_shipping_amount(current_doc){
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                
                method:'shipping_management.shipping_management.quotation.apply_shipping_rule_custom',
                args: {
                    curr_doc: current_doc,
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });

}





