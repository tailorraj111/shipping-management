frappe.ui.form.on('Sales Invoice', {
    onload:function(frm){
       
        if(frm.is_new()){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_sales_person",
                "args":{
                    "user":frappe.session.user
                },
                callback:function(res){
                    console.log(res)
                    if(res.message){
                        frm.set_value("sales_team",[])
                        frm.refresh_field("sales_team")
                        var newrow = frappe.model.add_child(frm.doc, "Sales Team", "sales_team");
                        newrow.sales_person = res.message;
                        newrow.allocated_percentage = 100;
                        cur_frm.script_manager.trigger("sales_person", newrow.doctype, newrow.name);
                       
                        frm.refresh_field("sales_team");
    
                    }
                }
    
            })
        }
        
    },
    setup: function(frm) {
		frm.set_query("shipping_method", function() {

			return {
				filters: {
					"item_group": "Shipping Item"
				}
			}
		});
    },
    department: function(frm){
        for(var i in frm.doc.items){
            frm.doc.items[i].department = frm.doc.department
        }
        frm.refresh_field("items")
    },
    division: function(frm){
        for(var i in frm.doc.items){
            frm.doc.items[i].division = frm.doc.division
        }
        frm.refresh_field("items")
    },
    validate:function(frm){
        frm.set_value("sales_person",frm.doc.sales_team[0].sales_person)
	    frm.refresh_field("sales_person")
        frappe.db.get_value("Customer",frm.doc.customer,"accounting_issue").then((res)=>{
           console.log(res.message.accounting_issue)
           if(res.message.accounting_issue){
               frappe.throw("Customer is having Accounting Issue. Please contact Accounts Department to resolve the issue. You can make Sales Order but Invoice is blocked.")
           }
           
        })
      
   },
   lr_no(frm){
       if(frm.doc.lr_no.length > 15){
        frappe.msgprint("Transport Reciept Number Should not Exceed 15 Character!")
        frm.set_value("lr_no","")
        frm.refresh_field("lr_no")
       }
   },
   set_warehouse(frm){
        if(frm.doc.set_warehouse == "Mumbai - T"){
            frm.set_value("naming_series","TI-M-")
            frm.refresh_field("naming_series")
        }else if(frm.doc.set_warehouse == "Shendra - T"){
            frm.set_value("naming_series","TI-S-")
            frm.refresh_field("naming_series")
        }
    },
    refresh :async function(frm) {
        // console.log(frm.doc.customer_loyalty);
        // var loyalty_type = frm.doc.customer_loyalty;
        let loyalty_type_res = await frappe.db.get_value("Customer",frm.doc.customer,"customer_loyalty");
        var loyalty_type = loyalty_type_res.message.customer_loyalty;
        console.log(loyalty_type)
        let btn = document.createElement('span');
    if(loyalty_type == "New"){
        {
            btn.innerText = 'New';
            btn.style = 'color: blue;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
         }
    } else if(loyalty_type == "Silver") {
         {
            btn.innerText = 'Silver';
            btn.style = 'color: green;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
         }
    } else if(loyalty_type == "Gold") {
        {
            btn.innerText = 'Gold';
            btn.style = 'color: orange;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
         }
    
    } else if(loyalty_type == "Platinum")
         {
            btn.innerText = 'Platinum';
            btn.style = 'color: red;font-size: 28px;float: left;font-weight: bold;';
            btn.id = "loayalty_indicator"
         }
            
                
        
            var tool_bar = $("div[data-page-route='Sales Invoice'] > .page-head > .container > .page-head-content > .page-title")
            console.log($("#loayalty_indicator"))
            $( "#loayalty_indicator" ).remove();
            tool_bar.append(btn)
            console.log(tool_bar)
       
        },


        // -----------------------------------shipping customization-----------------------------------
    shipping_method:async function(frm){

        if(frm.doc.shipping_method)
        {    
          
            if(!frm.doc.shipping_rule_custom){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
                frappe.throw("Please Select Shipping Rule First!")
            }
    
            if (!frm.doc.customer){
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");
            }

            if (!frm.doc.total_net_weight){
                frappe.msgprint("Shipping Cannot Apply To 0 Weighted Items")
                frm.set_value("shipping_method","")
                refresh_field("shipping_method");            
            }
        
        let shipping_amount = await calculate_shipping_amount(frm.doc)
        var found = 0
        // update Shipping Item If already Present
        
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                frm.doc.items[i]['item_code'] = frm.doc.shipping_method
                frm.doc.items[i]['qty'] = 1
                frm.doc.items[i]['rate'] = shipping_amount.message
                found = 1
                refresh_field("items");
            }
        }       


        // If not Shipping Item then add it.
        if (frm.doc.shipping_method && !found){
            console.log("add item")
            frappe.call({
                method:"frappe.client.get",
                args:{
                    "doctype":"Item",
                    "name":frm.doc.shipping_method
                },
                callback:function(res){

                    console.log(res.message)
                    var newrow = frappe.model.add_child(frm.doc, "Sales Invoice Item", "items");
                    newrow.item_code = res.message['name'];
                    newrow.item_name = res.message['item_name'];
                    newrow.item_group = res.message['item_group'];
                    newrow.description = res.message['description'];
                    newrow.stock_uom = res.message['stock_uom'];
                    newrow.uom = res.message['stock_uom'];
                    newrow.income_account = res.message['item_defaults'][0]['income_account'];
                    newrow.rate = shipping_amount.message
                    newrow.qty = 1
                    cur_frm.script_manager.trigger("rate", newrow.doctype, newrow.name);       
                }
            })

        }
    }

        // If shipping Method Field is Empty then remove it from table
        if (!frm.doc.shipping_method){
            for(var i = 0; i < frm.doc.items.length; i++){
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
        } 
        var shipping = 0
        for(var i = 0; i < frm.doc.items.length; i++){         
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                shipping = shipping + flt(frm.doc.items[i]['amount'])
            }
        }
        frm.set_value("shipping_charges",shipping)
        frm.set_value("item_total",flt(frm.doc.total - shipping))
        refresh_field("shipping_charges","item_total");
        
        

    },
    custom_discount_percentage:function(frm){
        if(frm.doc.item_total){
            frm.set_value("discount_amount",flt(frm.doc.item_total*frm.doc.custom_discount_percentage)/100)
        }else{
            frm.set_value("discount_amount",flt(frm.doc.total*frm.doc.custom_discount_percentage)/100)
        }
        refresh_field("discount_amount")
        // console.log(frm.doc.item_total)
    },
    custom_discount_amount:function(frm){
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] != "Shipping Item"){
                var distributed_amount = (frm.doc.custom_discount_amount * frm.doc.items[i]['net_amount'])/frm.doc.item_total
                frappe.model.set_value(frm.doc.items[i].doctype, frm.doc.items[i].name , "net_amount", frm.doc.items[i]['net_amount'] - distributed_amount);
                refresh_field("items")	
            }
        }

        
    },
   
    
    
})
cur_frm.cscript.rate = function(doc,cdt,cdn){
    update_item_total(doc,cdt,cdn)
    }

function update_item_total(doc,cdt,cdn){
    var shipping = 0
    for(var i = 0; i < doc.items.length; i++){
       
        if(doc.items[i]['item_group'] == "Shipping Item"){
            shipping = shipping + flt(doc.items[i]['amount'])
            
        }
    }
   
    cur_frm.set_value("shipping_charges",shipping)
    cur_frm.set_value("item_total",flt(doc.total - shipping))
    refresh_field("shipping_charges","item_total");
}



frappe.ui.form.on('Sales Invoice Item', {
   
    add_serial_number(frm,cdt,cdn){
	    console.log("here");
	    var row = locals[cdt][cdn];
	    if(row.initial_serial_number && row.serial_quantity){
	        var init_sn = row.initial_serial_number.substring(0, 4);
	        var in_sn = parseInt(row.initial_serial_number.substring(4))
	        var sn_str = []
	        
	        for(var i = 0; i < row.serial_quantity ; i++){
	            var digit = in_sn + i;
	            var len = row.initial_serial_number.substring(4).length;
	            var len_digit = digit.toString().length;
	            var temp_str = init_sn
	            while(len > len_digit){
	                temp_str += "0"
	                len_digit += 1;
	            }
	            
	            sn_str.push(temp_str + digit)
	             console.log(temp_str + digit)
	        }
	        if(row.serial_no){
	            row.serial_no = row.serial_no + "\n" + sn_str.join("\n")
	        }
	        else{
	            row.serial_no = sn_str.join("\n")
	        }
	        
	        frm.refresh_field("items")
	    }
	},
	clear_serial_number(frm,cdt,cdn){
	    var row = locals[cdt][cdn];
	    row.serial_no = ""
	    frm.refresh_field("items")
        frm.dirty()
	},
    items_add:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
            refresh_field("shipping_method");
            for(var i = 0; i < frm.doc.items.length; i++){
                if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                    frm.doc.items.pop(frm.doc.items[i])
                    refresh_field("items");
                }
            }
       
    },
    items_remove:function(frm,cdt,cdn){
        cur_frm.set_value("shipping_method","")
        refresh_field("shipping_method");
        for(var i = 0; i < frm.doc.items.length; i++){
            if(frm.doc.items[i]['item_group'] == "Shipping Item"){
                frm.doc.items.pop(frm.doc.items[i])
                refresh_field("items");
            }
        }
        
    }
})

async function calculate_shipping_amount(current_doc){
    return new Promise(function (resolve, reject) {
        try {
            frappe.call({
                
                method:'shipping_management.shipping_management.quotation.apply_shipping_rule_custom',
                args: {
                    curr_doc: current_doc,
                },
                callback: resolve
            });
        } catch (e) { reject(e); }
    });

}