frappe.ui.form.on('Address', {
    refresh(frm) {
        // your code here
    },
    validate(frm) {
        if(frm.doc.is_primary_address === 1 && frm.doc.gstin){
            frappe.call({
            method: 'india_compliance.gst_india.utils.gstin_info.get_gstin_info',
            args: {
                "gstin": frm.doc.gstin
            },
            callback: function(r) {
                    if (r.message && frm.doc.is_primary_address === 1) {
                        frm.set_value("address_line1", r.message.permanent_address.address_line1);
                        frm.set_value("address_line2", r.message.permanent_address.address_line2);
                        frm.set_value("city", r.message.permanent_address.city);
                        frm.set_value("state", r.message.permanent_address.state);
                        frm.set_value("pincode", r.message.permanent_address.pincode);
                        frm.refresh_field("address_line1","address_line2","city","state","pincode");
                    }
                }
            });
        }
        
    }
});
