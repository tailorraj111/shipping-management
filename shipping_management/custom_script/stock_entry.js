frappe.ui.form.on('Stock Entry', {
	refresh:function(frm){
		cur_frm.fields_dict['serial_number_voucher'].get_query = function(doc) {
			return {
				filters: {
					"work_order": frm.doc.work_order
				}
			}
		}


		frm.set_query('custom_reference_batch_bundle', 'items', () => {
            return {
                filters: {
                    'voucher_no': frm.doc.name
                }
            }
        })
		
	},

	// serial_number_voucher: function(frm){
	// 	console.log(frm.doc.serial_number_voucher)
	// 	if(frm.doc.serial_number_voucher){
	// 		frappe.call({
    //             "method":"shipping_management.shipping_management.stock_entry.get_serial",
    //             "args":{
    //                 "voucher_no":frm.doc.serial_number_voucher
    //             },
	// 			callback: function(res){

	// 				console.log(res.message)
					
	// 				for(var i in frm.doc.items){
	// 					if(frm.doc.items[i].is_finished_item == 1){
	// 						frm.doc.items[i].batch_no = res.message[2]
	// 						if(frm.doc.items[i].item_code == res.message[1]){
	// 							var qty = frm.doc.items[i].qty;
	// 							var serials = ""
	// 							for(var j in res.message[0]){
	// 								if(j == qty){
	// 									break;
	// 								}
	// 								serials += res.message[0][j] + "\n"
	// 							}

	// 							frm.doc.items[i].serial_no = serials
	// 							refresh_field("Items")
	// 						}
	// 						else{
	// 							frappe.msgprint("Item Code do not match, Select another Voucher")
	// 						}
	// 					}
	// 					if(frm.doc.items[i].item_code == res.message[4]){
	// 						var serials = ""
	// 							for(var j in res.message[3]){
	// 								if(j == qty){
	// 									break;
	// 								}
	// 								serials += res.message[3][j] + "\n"
	// 							}
	// 							frm.doc.items[i].serial_no = serials
	// 							refresh_field("Items")
	// 					}
	// 				}
	// 			}
    //         })
	// 	}
	// },
	after_workflow_action(frm) {
		// your code here
		console.log("here")
		if(frm.doc.workflow_state == "Material Requested"){
		    console.log("here1")
		    frappe.db.set_value("Work Order",frm.doc.work_order,"status","Material Requested")
		}
	},
    on_submit : function(frm) {        
        if(frm.doc.outgoing_stock_entry && frm.doc.stock_entry_type == "Receive at Warehouse"){
            frappe.call({
                "method":"shipping_management.shipping_management.stock_entry.send_email_notification",
                "args":{
                    "stock_entry":frm.doc.name,
                    "out_going_entry":frm.doc.outgoing_stock_entry
                }
            })
        }
    },
    stock_entry_type(frm){

	    if(frm.doc.stock_entry_type == "Material Issue"){
	        frm.set_value("naming_series","MI-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Receipt"){
	        frm.set_value("naming_series","MR-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Transfer"){
	        frm.set_value("naming_series","MT-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Transfer for Manufacture"){
	        frm.set_value("naming_series","MTM-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Material Consumption for Manufacture"){
	        frm.set_value("naming_series","MCM-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Manufacture"){
	        frm.set_value("naming_series","MF-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Send to Warehouse"){
	        frm.set_value("naming_series","SW-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Receive at Warehouse"){
	        frm.set_value("naming_series","RW-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "For Rejection"){
	        frm.set_value("naming_series","REJ-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Replacement"){
	        frm.set_value("naming_series","REP-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Repacking"){
	        frm.set_value("naming_series","RPK-")
	        frm.refresh_field("naming_series")
	    }else if(frm.doc.stock_entry_type == "Stock JV"){
	        frm.set_value("naming_series","SJV-")
	        frm.refresh_field("naming_series")
	    }
	},
	department: function(frm){
        for(var i in frm.doc.items){
            frm.doc.items[i].department = frm.doc.department
        }
        frm.refresh_field("items")
    },
    division: function(frm){
        for(var i in frm.doc.items){
            frm.doc.items[i].division = frm.doc.division
        }
        frm.refresh_field("items")
    },

})

frappe.ui.form.on('Stock Entry Detail', {
	refresh(frm) {
		// your code here
	},
	add_serial_number(frm,cdt,cdn){
	    console.log("here");
	    var row = locals[cdt][cdn];
	    if(row.initial_serial_number && row.serial_quantity){
	        var init_sn = row.initial_serial_number.substring(0, 4);
	        var in_sn = parseInt(row.initial_serial_number.substring(4))
	        var sn_str = []
	        
	        for(var i = 0; i < row.serial_quantity ; i++){
	            var digit = in_sn + i;
	            var len = row.initial_serial_number.substring(4).length;
	            var len_digit = digit.toString().length;
	            var temp_str = init_sn
	            while(len > len_digit){
	                temp_str += "0"
	                len_digit += 1;
	            }
	            
	            sn_str.push(temp_str + digit)
	             console.log(temp_str + digit)
	        }
	        if(row.serial_no){
	            row.serial_no = row.serial_no + "\n" + sn_str.join("\n")
	        }
	        else{
	            row.serial_no = sn_str.join("\n")
	        }
	        
	        frm.refresh_field("items")
	    }
	},
	clear_serial_number(frm,cdt,cdn){
	    var row = locals[cdt][cdn];
	    row.serial_no = ""
	    frm.refresh_field("items")
        frm.dirty()
	},
	custom_create_serial_batch_bundle_from_serial_no_:function(frm,cdt,cdn){
		var row = locals[cdt][cdn]
		if(row.serial_and_batch_bundle){
			frappe.throw("Already Value Set In Serial And Batch Bundle Field")
		}
		if(!frm.doc.serial_number_voucher){
			frappe.throw("Please Set Value In Serial Number Voucher Field.")
		}
		frappe.call({
			method: 'shipping_management.shipping_management.stock_entry.create_serial_batch_bundle',
			args:{
				'item_code':row.item_code,
				'qty':row.qty,
				'child_name':row.name,
				'warehouse' : row.t_warehouse,
				'rate':row.basic_rate,
				'total_amount':row.amount,
				'serial_number_voucher':frm.doc.serial_number_voucher,
				'parent':row.parent,
				'parenttype':row.parenttype

			},
			callback: function(res){
				console.log(res)
				frm.reload_doc();
				
			}
		});
	},
	
	custom_create_bundle_from_reference:function(frm,cdt,cdn){
        var row = locals[cdt][cdn]

		frappe.call({
			
			method: 'shipping_management.shipping_management.stock_entry.duplicate_serial_batch_bundle',
			args:{
				'item_code':row.item_code,
				'sn_bundle':row.custom_reference_batch_bundle

			},
			callback: function(res){
				console.log(res)
				update_bundle_entries(res.message, row.t_warehouse,row,frm);
				// frm.reload_doc();
				
			}
		});
    }
})

function update_bundle_entries(entries, warehouse,item,frm) {
		

	frappe.call({
			method: "erpnext.stock.doctype.serial_and_batch_bundle.serial_and_batch_bundle.add_serial_batch_ledgers",
			args: {
				entries: entries,
				child_row: item,
				doc: frm.doc,
				warehouse: warehouse,
			},
		})
		.then((r) => {
			console.log(r);
			item.serial_and_batch_bundle = r.message.name;
			frm.save();
			frappe.msgprint(__("Trueview Serial and Batch Bundle Created"));
			
		});
}