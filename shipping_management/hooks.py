# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "shipping_management"
app_title = "Shipping Management"
app_publisher = "Raj Tailor"
app_description = "Shipping Functions"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "tailorraj111@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/shipping_management/css/shipping_management.css"
# app_include_css = "/assets/shipping_management/css/shipping_management.css"
app_include_js = "/assets/shipping_management/js/shipping_management.js"

# include js, css files in header of web template
# web_include_css = "/assets/shipping_management/css/shipping_management.css"
# web_include_js = "/assets/shipping_management/js/shipping_management.js"
app_logo_url = "/files/tvlogo.png"

# include js in page
# page_js = {"page" : "public/js/file.js"}

doctype_js = {
    "Sales Invoice": ["custom_script/sales_invoice.js"],
	"Purchase Invoice": ["custom_script/purchase_invoice.js"],
    "Sales Order": ["custom_script/sales_order.js"],
    "Quotation": ["custom_script/quotation.js"],
    "Delivery Note": ["custom_script/delivery_note.js"],
	"Landed Cost Voucher":["custom_script/landed_cost_voucher.js"],
	"Stock Entry":["custom_script/stock_entry.js"],
	# "Payroll Entry":["custom_script/payroll_entry.js"],
	"Customer":["custom_script/customer.js"],
	"Issue":["custom_script/issue.js"],
	"Material Request":["custom_script/material_request.js"],
	"Work Order":["custom_script/work_order.js"],
	"Purchase Receipt": ["custom_script/purchase_receipt.js"],
	"Journal Entry": ["custom_script/journal_entry.js"],

}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

fixtures = [
	"Print Format",
	{"dt":"Item Group", "filters": [["name", "=", "Shipping Item"]]},
	{"dt":"Custom Field", "filters": [["name", "in", ["Quotation-brand","Delivery Note-remark","Supplier-tds_applicable","Sales Order Item-reference_dn","Sales Order Item-reference_dt","Item-s1","Item-item_version","Item-s0","Sales Order-credit_type","Sales Order-make_payment_c","Sales Order-payment_entry","Sales Order-payment_date","Sales Order-refresh_outstanding","Sales Order-approved_sales_order","Sales Order-current_ledger_balance","Sales Invoice-number_of_box","Sales Invoice-sales_person","Sales Order-sales_person","Customer-credit_type","Item-zero_duty","Stock Entry Detail-brand","Stock Entry-workflow_state","Item-stock_issue","Sales Invoice-qrcode_image","Sales Invoice-signed_qr_code","Sales Invoice-signed_einvoice","Sales Invoice-eway_bill_cancelled","Sales Invoice-irn_cancelled","Sales Invoice-ack_date","Sales Invoice-ack_no","Sales Invoice-irn","Sales Invoice-transporter_address_display","Sales Invoice-transporter_address","Sales Order-rr_reference","Sales Invoice-rr_reference","Customer-in_transit_warehouse","Customer-faulty_warehouse","Customer-fresh_warehouse","Customer-mapped_warehouse","P2P Request-workflow_state","Quotation-total_with_outstanding_c","Sales Order-credit_limit","Sales Order-commitment_date","Sales Order-current_outstanding","Employee-aadhar_card_number","Salary Slip-employer_contribution_custom","Salary Slip-actual_deduction_custom","Sales Order-approved_by","Employee-insurance","Customer-customer_loyalty","Sales Order-outstanding_limit","Sales Order-overdue_amount","Customer-accounting_issue","Sales Order-column_break_28","Sales Order-accounting_status","Sales Order-paid_amount","Salary Detail-employer_contri","Employee-has_nps","Salary Slip-pr_account_number_pran","Salary Slip-esi_number","Salary Slip-column_break_16","Salary Slip-pf_account_number","Salary Slip-universal_account_number_uan","Salary Slip-income_tax_number_pan","Salary Slip-section_break_12","Employee-pr_account_number_pran","Employee-esi_number","Employee-column_break_21","Employee-pf_account_number","Employee-universal_account_number_uan","Employee-income_tax_number_pan","Employee-section_break_17","Salary Slip-custom_net_pay","Employee-tds_amount","Employee-has_pf_account","Sales Order-rejection_reason","Sales Order-mode_of_payment","Sales Order-payment_remarks","Salary Component-employer_contribution","Payroll Entry-custome_month","Dealer Registration-workflow_state","Sales Invoice-customer_remark","Warehouse-allow_negative_stock","Bank Account-ifsc_code","Quotation-select_bank","Quotation-show_bank_details","Quotation-bank_details","Sales Invoice-remark_if_any","Sales Invoice-remark","Sales Order-remark_if_any","Sales Order-remark","Quotation-remark_if_any","Quotation-remark","Sales Order-credit_limit_crossed","Sales Invoice-sales_executive_name","Sales Invoice-sales_executive","Sales Order-sales_executive_name","Item-workflow_state","Sales Invoice-workflow_state","Salary Slip-custome_month","Delivery Note-number_of_box","Sales Order-sales_executive","Delivery Note Item-item_brand","Sales Order Item-item_brand","Sales Order Item-reference_code","Delivery Note Item-reference_code","Quotation Item-item_brand","Quotation Item-reference_code","Quotation-shipping_rule_custom","Purchase Taxes and Charges-custom_amount","Purchase Invoice Item-is_nil_exempt","Delivery Note-shipping_rule_custom","Sales Order-shipping_rule_custom","Sales Invoice-shipping_rule_custom","Issue-customer_group","Issue-workflow_state","ToDo-time","Issue-call_log_details","Issue-call_details","Issue-support_engineer_number","Issue-support_engineer_name","Issue-item_group","Job Card-section_break_0","Job Card-brand","Work Order-brand","Sales Invoice Item-reference_code","Production Plan Item-remark","Production Plan Item-brand","Sales Invoice Item-item_brand","Landed Cost Item-assessable_value","Delivery Note-shipping_bill_date","Delivery Note-shipping_bill_number","Landed Cost Voucher-total_igst_amount","Landed Cost Voucher-total_custom_duty_amount","Landed Cost Voucher-is_domestic_invoice","Landed Cost Voucher-add_charges","Landed Cost Item-igst_amount","Landed Cost Item-custom_duty_amount","Landed Cost Item-igst_rate","Landed Cost Item-custom_duty","Purchase Receipt Item-custom_duty","Customer-pan_","Customer-column_break_152","Job Card-seq","Customer-workflow_state","Customer-price_list","Delivery Note-shipping_charges","Delivery Note-custom_discount_amount","Delivery Note-custom_discount_percentage","Delivery Note-shipping_method","Delivery Note-item_total","Sales Order-custom_discount_amount","Sales Order-custom_discount_percentage","Sales Order-shipping_charges","Sales Order-item_total","Quotation-custom_discount_amount","Quotation-custom_discount_percentage","Quotation-shipping_charges","Quotation-item_total","Sales Invoice-item_total","Sales Invoice-custom_discount_amount","Sales Invoice-custom_discount_percentage","Sales Invoice-shipping_charges","Quotation Item-available_batch_qty_at_warehouse","Item-minimum_weight","Sales Order-workflow_state","Quotation-shipping_method","Sales Order-shipping_method","Sales Invoice-shipping_method","Quotation-brand","Item-reference_code","Customer-credit_limit_applied_for","Customer-expected_monthly_business_with_us","Customer-credit_limit_requirement","Customer-current_credit_limit_3","Customer-average_monthly_business_3","Customer-supplier_mobile_number_3","Customer-supplier_email_3","Customer-supplier_contact_person_name_3","Customer-supplier_city_3","Customer-supplier_name_3","Customer-column_break_142","Customer-current_credit_limit_2","Customer-average_monthly_business_2","Customer-supplier_mobile_number_2","Customer-supplier_email_2","Customer-supplier_contact_person_name_2","Customer-supplier_city_2","Customer-supplier_name_2","Customer-column_break_134","Customer-current_credit_limit_1","Customer-average_monthly_business_1","Customer-supplier_mobile_number_1","Customer-supplier_email_1","Customer-supplier_contact_person_name_1","Customer-supplier_city1","Customer-supplier_name_1","Customer-trade_or_supplier_reference","Customer-owner_3_pass_port_size_photo","Customer-owner_3","Customer-column_break_123","Customer-owner_2_pass_port_size_photo","Customer-owner_2","Customer-owner_1_pass_port_size_photo","Customer-owner_1","Customer-details_of_owners_or_partners_or_directors_","Customer-internal_rating","Customer-credit__amount","Customer-credit__days","Material Request-workflow_state","Stock Entry Detail-gst_hsn_code","Purchase Order-workflow_state","Item Alternative-workflow_state","Customer-for_office_use","Customer-turnover_2_copy","Customer-turnover_1_copy","Customer-column_break_110","Customer-office_space_","Customer-no_of_branches","Customer-no_of_employees","Customer-turnover_two","Customer-turnover_one","Customer-turnover__for_last_2_years","Customer-nature_of_business","Customer-nature_of_business_or_type_of_customer","Customer-replacement_department","Customer-column_break_100","Customer-logistic_department","Customer-column_break_98","Customer-sales_department","Customer-column_break_96","Customer-account_department","Customer-department_wise_contact_details","Customer-delivery_mobile","Customer-delivery_state","Customer-delivery_city","Customer-column_break_90","Customer-delivery_office_phone","Customer-delivery_pin","Customer-delivery_address","Customer-delivery_details","Customer-billing_mobile","Customer-billing_state","Customer-billing_city","Customer-column_break_82","Customer-billing_office_phone_","Customer-billing_pin","Customer-billing_address","Customer-billing_details","Customer-bank_account_details_2","Customer-bank_account_details_1","Customer-bank_details","Customer-registered_mobile","Customer-registered_state","Customer-registered_city","Customer-udyog_aadhaar_scanned_copy","Customer-pan_number_scanned_copy","Customer-gst_number_scanned_copy","Customer-cin_number_scanned_copy","Customer-column_break_67","Customer-registered_office_phone","Customer-registered_pin","Customer-registered_address"]]]}
	,
	{"dt":"Workflow", "filters": [["document_type", "in", ["Sales Order Multilevel","Sales Invoice Flow","Stock Entry","Item Approval","Customer Approval Flow"]]]},
	"Property Setter"]
# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "shipping_management.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "shipping_management.install.before_install"
# after_install = "shipping_management.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "shipping_management.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"Sales Invoice" :{
		"validate": "shipping_management.shipping_management.sales_invoice.validate"
		
		
	},
    "Quotation" :{
		"validate": "shipping_management.shipping_management.quotation.validate",
		
	},
     "Sales Order" :{
		"validate": "shipping_management.shipping_management.sales_order.validate",
		"after_insert":["shipping_management.shipping_management.sales_order.after_insert","shipping_management.shipping_management.sales_order.check_credit_on_insert"],
		"on_submit":"shipping_management.shipping_management.sales_order.on_submit"	,
		"on_update":"shipping_management.shipping_management.sales_order.on_update"
	},
    "Delivery Note" :{
		"validate": "shipping_management.shipping_management.delivery_note.validate",
		
	},
	"Salary Slip" :{
		"validate": "shipping_management.shipping_management.salary_slip.validate",
		
	},
	"Stock Entry":{
		"validate": "shipping_management.shipping_management.stock_entry.validate"
	},

	"Pick List":{
		"on_submit":"shipping_management.shipping_management.pick_list.validate"
	},
	 "Purchase Invoice" :{
		"on_submit":"shipping_management.shipping_management.purchase_invoice.on_submit",
	},
	"Purchase Receipt" :{
		"on_submit":"shipping_management.shipping_management.purchase_receipt.on_submit",
	},
	"Purchase Order" :{
		"validate": "shipping_management.shipping_management.purchase_order.validate"	
	},
    "Issue":{
        "validate":"shipping_management.override.issue.validate"
	}
}
# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

scheduler_events = {
	"cron": {
        # "14 * * * *": [
        #     "shipping_management.shipping_management.quantity_manage.process_incorrect_balance_qty"
        # ],
		"0 0 * * *":[
			"shipping_management.shipping_management.quantity_manage.clear_all_reposting"
		]
    },
	# "hourly_long": [
	# 	"shipping_management.quantity_management.process_incorrect_balance_qty"
	# ]
}

scheduler_events = {
	# "all": [
	# 	"shipping_management.tasks.all"
	# ],
	# "daily": [
	# 	"shipping_management.tasks.daily"
	# ],
	# "hourly": [
	# 	"shipping_management.shipping_management.doctype.stock_check.stock_check.clear_queue"
	# ]
	# "weekly": [
	# 	"shipping_management.tasks.weekly"
	# ]
	# "monthly": [
	# 	"shipping_management.tasks.monthly"
	# ]
}

# scheduler_events = {
# 	"hourly_long": [
# 		"shipping_management.quantity_management.process_incorrect_balance_qty"
# 	]
# }
# Testing
# -------

# before_tests = "shipping_management.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "shipping_management.event.get_events"
# }

