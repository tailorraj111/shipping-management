from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Couriour Details"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Courier Detail",
					"description": _("Courier Detail."),
					"onboard": 1,
				}
				
			]
		}		
	]
