from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Issues"),
			"items": [
				{
					"type": "doctype",
					"name": "Query and Complaint",
					"description": _("Complaint"),
				},
			]
		},
		
	]