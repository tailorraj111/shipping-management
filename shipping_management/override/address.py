import frappe

def before_validate(self,method):
    if self.is_primary_address:
        existing_primary_addresses = frappe.get_all(
                'Address',
                filters={
                    'link_name': self.links[0].link_name,
                    'is_primary_address': 1,
                    'name': ['!=', self.name]
                }
            )
            
        if existing_primary_addresses:
            frappe.throw("Customer already has a primary address.") 