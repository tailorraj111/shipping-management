from __future__ import unicode_literals
import json
import frappe, erpnext
from frappe import _, scrub
from frappe.utils import cint, flt, round_based_on_smallest_currency_fraction
from erpnext.controllers.accounts_controller import validate_conversion_rate, \
	validate_taxes_and_charges, validate_inclusive_tax
from num2words import num2words

def validate(self, method):
	validate_turnover(self)

def validate_turnover(self):
	if any(d.account_head == 'TDS 194Q - TDS on Purchase of Goods - T' for d in self.taxes):
		for item in self.taxes:
			if item.account_head == "TDS 194Q - TDS on Purchase of Goods - T":
				item.tax_amount = 0

	tds_flag = frappe.db.get_value("Supplier",self.supplier,"tds_applicable")
	if tds_flag:
		taxable_amount = float(self.grand_total)
		tax_amount = (taxable_amount * 0.1)/100
	
		if not any(d.account_head == 'TDS 194Q - TDS on Purchase of Goods - T' for d in self.taxes):
			self.append("taxes",{
				"category": "Valuation",
				"add_deduct_tax":"Add",
				"charge_type": "On Net Total",
				"account_head":"TDS 194Q - TDS on Purchase of Goods - T",
				"description":"TDS 194Q - TDS on Purchase of Goods - T",
				"tax_amount":tax_amount,
				"base_tax_amount":tax_amount,
				"base_tax_amount_after_discount_amount":tax_amount,
				"tax_amount_after_discount_amount":tax_amount,
				"total": taxable_amount + tax_amount
			})

		else:
			for item in self.taxes:
				if item.account_head == "TDS 194Q - TDS on Purchase of Goods - T":
					taxable_amount = float(self.grand_total) - item.tax_amount_after_discount_amount
					tax_amount = (taxable_amount * 0.1)/100
					item.tax_amount = tax_amount
		
		self.grand_total = taxable_amount + tax_amount
		self.taxes_and_charges_added += tax_amount
		self.total_taxes_and_charges += tax_amount
		self.rounding_adjustment = "{:.4f}".format(round(taxable_amount + tax_amount) - (taxable_amount + tax_amount))
		self.rounded_total = "{:.4f}".format(round(taxable_amount + tax_amount))
		self.in_words = "INR " + str.title(num2words(round(taxable_amount + tax_amount), lang='en_IN')) + " Only"
