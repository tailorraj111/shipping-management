import frappe
from erpnext.hr.doctype.employee.employee import get_holiday_list_for_employee
import json
from frappe.utils import date_diff


@frappe.whitelist()
def get_leave_amounts(payroll_doc):
    doc = json.loads(payroll_doc)
    # frappe.msgprint(str(payroll_doc))
    for item in doc['employees']:
        # frappe.msgprint(str(item['employee']))
        holiday_list= get_holidays_for_employee(item['employee'],doc['start_date'],doc['end_date'])
        # frappe.msgprint(str(holiday_list))
        employee_attendance = frappe.db.get_all("Attendance",filters = [["employee","=",item['employee']],["attendance_date","between",[doc['start_date'],doc['end_date']]],["docstatus","=",1]],fields=["status","leave_type","working_hours","attendance_date"])
        holiday_number = 0
        for data in employee_attendance:
            if str(data.attendance_date) in holiday_list:
                holiday_number = holiday_number + 1
        if holiday_number > 0:
            holiday_pay = holiday_number * get_employee_perday(item['employee'],doc['start_date'],doc['end_date'])
            holiday_pay_amt = create_component(doc['start_date'],item['employee'],holiday_pay)
            # frappe.db.set_value("Payroll Employee Detail",item["name"],"work_on_holiday",holiday_number)
            # frappe.db.set_value("Payroll Employee Detail",item["name"],"work_on_holiday_amount",holiday_pay_amt)

            frappe.msgprint("Holiday Component Created For Employee {} of amount {}".format(str(item['employee']),str(holiday_pay_amt)))
    
        


def create_component(posting_date,employee,value):
    company = frappe.db.get_value('Employee', employee, 'company')
    additional_salary = frappe.new_doc('Additional Salary')
    additional_salary.employee = employee
    additional_salary.salary_component = "Leave Encashment"
    additional_salary.amount = value
    additional_salary.payroll_date = posting_date
    additional_salary.company = company
    additional_salary.submit()
    return additional_salary.amount

def get_employee_perday(employee,start_date,end_date):
    # frappe.msgprint()
    total_days = int(date_diff(end_date, start_date)) + 1
    # frappe.throw(str(total_days))
    gross = frappe.db.get_value("Salary Structure Assignment",{"employee":employee,"docstatus":1},"base")

    return float(gross/int(total_days))

def get_holidays_for_employee(employee,start_date, end_date):
		holiday_list = get_holiday_list_for_employee(employee)
		holidays = frappe.db.sql_list('''select holiday_date from `tabHoliday`
			where
				parent=%(holiday_list)s
				and holiday_date >= %(start_date)s
				and holiday_date <= %(end_date)s''', {
					"holiday_list": holiday_list,
					"start_date": start_date,
					"end_date": end_date
				})

		holidays = [str(i) for i in holidays]

		return holidays