// Copyright (c) 2020, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Courier Detail', {
	refresh(frm) {
	    $("[data-fieldname=shipping_address]" ).css("height", "220px");
	    $("[data-fieldname=shipping_address]" ).css("overflow", "overlay");
	   // msgprint('called on laod')
		// your code here
		    // ============= get supplier whose customer group is transport ===============
    cur_frm.set_query('courier_name', function () {
    return {
        filters: {
            'supplier_group': 'Transport Companies'
        }
    }
});
	}
})