// Copyright (c) 2021, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Stock Check', {
	// refresh: function(frm) {

	// }
	delete_duplicate_role: function(frm){
		frappe.call({
			method: "delete_role",
			doc: frm.doc,
			callback: function(res){
				console.log(res)
			}
		});
	},
	get_queue: function(frm){
		frappe.call({
			method: "reposting_stock",
			doc: frm.doc,
			callback: function(res){
				console.log(res)
			}
		});
	}
});
