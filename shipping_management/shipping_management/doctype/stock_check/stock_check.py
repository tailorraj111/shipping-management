# Copyright (c) 2021, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe import publish_progress
from frappe.model.document import Document

from shipping_management.shipping_management.quantity_manage import process_incorrect_balance_qty_on_click
# from console import console
#from frappe.core.page.background_jobs.background_jobs import get_info

from frappe.utils.background_jobs import  get_jobs
# from erpnext.stock.doctype.repost_item_valuation.repost_item_valuation import RepostItemValuation
from erpnext.stock.doctype.repost_item_valuation.repost_item_valuation import repost




# import frappe
from frappe import _
from frappe.utils import cint
# from console import console

import erpnext
from erpnext.accounts.utils import (
	update_gl_entries_after,
)
from erpnext.stock.stock_ledger import repost_future_sle
# from console import console


class StockCheck(Document):
    @frappe.whitelist()
    def reposting_stock(self):
        data = frappe.get_last_doc('Repost Item Valuation', filters={"status": "Queued"})
        # frappe.publish_realtime('msgprint', data.allow_negative_stock)
        from frappe.utils import nowdate, now_datetime
        frappe.db.set_value('Stock Check', 'Stock Check', 'last_reposted', now_datetime())
        data.allow_negative_stock = 1
        
        doc = data
        repost_sl_entries(doc)
        repost_gl_entries(doc)
        frappe.db.set_value('Repost Item Valuation', doc.name, 'status', 'Completed')
        frappe.db.commit()

    @frappe.whitelist()
    def delete_role(self):
        doc = frappe.db.sql("""
            select name from `tabDocType`
            """,as_dict = True)

        roles = frappe.db.sql("""
                select name from `tabRole`
                """, as_dict = 1)
        
        doc_length = len(doc)
        for k, i in enumerate(doc):
            for j in roles:
                doctype = i.name
                role_data = j.name
                level = 0

                while level <= 9:
                    role = frappe.db.sql("""
                    select name from `tabCustom DocPerm` where parent = '%s' and role = '%s' and permlevel = %s limit 1
                    """%(doctype, role_data, level), as_list = 1)

                    if role == []:
                        level += 1
                        continue

                    role1 = frappe.db.sql("""
                    select name from `tabCustom DocPerm` where parent = '%s' and role = '%s' and permlevel = %s and name not in ('%s')
                    """%(doctype, role_data, level, role[0][0]),as_dict = True)

                    str_roles = ""
                    for item in role1:
                        frappe.db.sql("""delete from `tabCustom DocPerm` where name = %s""", (item.name))

                    level += 1
        
            progress = k / doc_length * 100
            publish_progress(percent=progress, title="Deleting the Duplicates")


        frappe.msgprint("Duplication deleted Successfully")

    @frappe.whitelist()
    def check_stock(self):
        # frappe.db.sql("""update `tabSales Taxes and Charges` set description = "IGST" where description = "IGST @ 18.0" """)
        # frappe.db.sql("""update `tabSales Taxes and Charges` set description = "SGST" where description = "SGST @ 9.0" """)
        # frappe.db.sql("""update `tabSales Taxes and Charges` set description = "CGST" where description = "CGST @ 9.0" """)
        # frappe.db.commit()
        process_incorrect_balance_qty_on_click()

    @frappe.whitelist()
    def get_queue(self):
        # console(frappe.local.site).log()
        queued_jobs = get_jobs(site=frappe.local.site, queue="long")
        # console(queued_jobs).log()
        # enqueued_jobs = [d.get("job_name") for d in get_info()]
        # console(enqueued_jobs).log    
        # for data in enqueued_jobs:
        #     console(data).log()
        

        # item = frappe.db.get_list("Item",{},"name")
		# frappe.msgprint(str(item))
		# stock_ledger_query = frappe.db.sql("select qty_after_transaction from `tabStock Ledger Entry` se where se.qty_after_transaction")

@frappe.whitelist()
def repost_sl_entries(doc):
	if doc.based_on == 'Transaction':
		repost_future_sle(doc=doc, voucher_type=doc.voucher_type, voucher_no=doc.voucher_no,
			allow_negative_stock=doc.allow_negative_stock, via_landed_cost_voucher=doc.via_landed_cost_voucher)
	else:
		repost_future_sle(args=[frappe._dict({
			"item_code": doc.item_code,
			"warehouse": doc.warehouse,
			"posting_date": doc.posting_date,
			"posting_time": doc.posting_time
		})], allow_negative_stock=doc.allow_negative_stock, via_landed_cost_voucher=doc.via_landed_cost_voucher)

@frappe.whitelist()
def repost_gl_entries(doc):
	if not cint(erpnext.is_perpetual_inventory_enabled(doc.company)):
		return

	if doc.based_on == 'Transaction':
		ref_doc = frappe.get_doc(doc.voucher_type, doc.voucher_no)
		items, warehouses = ref_doc.get_items_and_warehouses()
	else:
		items = [doc.item_code]
		warehouses = [doc.warehouse]

	update_gl_entries_after(doc.posting_date, doc.posting_time,
		warehouses, items, company=doc.company)
	# console("acc entry reposted").log()

@frappe.whitelist()
def clear_queue():
    queued_repost = frappe.db.get_list("Repost Item Valuation",{"status":"Queued","correction_entry":1},"name")
    single_doc = queued_repost[0]
    repost_doc = frappe.get_doc("Repost Item Valuation",single_doc["name"])
    frappe.enqueue(repost, timeout=1800, queue='long',
    job_name='repost_sle', now=True, doc=repost_doc)
    # frappe.msgprint(str(queued_repost))
    # for item in queued_repost:
    #     repost_doc = frappe.get_doc("Repost Item Valuation",item["name"])
    #     frappe.enqueue(repost, timeout=1800, queue='long',
    #     job_name='repost_sle', now=True, doc=repost_doc)
    #     # frappe.msgprint(str(queued_repost))
    



# @frappe.whitelist()
# def get_data(filters):
#	 console("here").log()
#	 filters = frappe._dict(filters)
#     data = get_stock_ledger_entries(filters)
#     itewise_balance_qty = {}

#     for row in data:
#         key = (row.item_code, row.warehouse)
#         itewise_balance_qty.setdefault(key, []).append(row)

#     res = validate_data(itewise_balance_qty)
#     return res


# def validate_data(itewise_balance_qty):
#     res = []
#     for key, data in itewise_balance_qty.items():
#         row = get_incorrect_data(data)
#         if row:
#             res.append(row)

#     return res


# def get_incorrect_data(data):
#     balance_qty = 0.0
#     for row in data:
#         balance_qty += row.actual_qty
#         if row.voucher_type == "Stock Reconciliation" and not row.batch_no:
#             balance_qty = flt(row.qty_after_transaction)

#         row.expected_balance_qty = balance_qty
#         if abs(flt(row.expected_balance_qty) - flt(row.qty_after_transaction)) > 0.5:
#             row.differnce = abs(
#                 flt(row.expected_balance_qty) - flt(row.qty_after_transaction)
#             )

#             console(row).log()
#             return row


# def get_stock_ledger_entries(report_filters):
#     filters = {"is_cancelled": 0}
#     fields = [
#         "name",
#         "voucher_type",
#         "voucher_no",
#         "item_code",
#         "actual_qty",
#         "posting_date",
#         "posting_time",
#         "company",
#         "warehouse",
#         "qty_after_transaction",
#         "batch_no",
#     ]

#     for field in ["warehouse", "item_code", "company"]:
#         if report_filters.get(field):
#             filters[field] = report_filters.get(field)

#     return frappe.get_all(
#         "Stock Ledger Entry",
#         fields=fields,
#         filters=filters,
#         order_by="timestamp(posting_date, posting_time) asc, creation asc",
#     )


# def process_incorrect_balance_qty():
#     data = get_data({})
#     if len(data) > 0:
#         rec = frappe._dict(data[0])
#         doc = frappe.new_doc("Repost Item Valuation")
#         doc.based_on = "Transaction"
#         doc.voucher_type = rec.voucher_type
#         doc.voucher_no = rec.voucher_no
#         doc.posting_date = rec.posting_date
#         doc.posting_time = rec.posting_time
#         doc.company = rec.company
#         doc.warehouse = rec.warehouse
#         doc.allow_negative_stock = 1
#         doc.docstatus = 1
#         doc.insert(ignore_permissions=True)
#         frappe.db.commit()
