// Copyright (c) 2021, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Secondary Sales Report', {
	// refresh: function(frm) {

	// }
	setup(frm){
		frm.set_query("billing_point",function(){
			return{
				filters:{
					"customer_group":["in",["Distributor","Super Distributor"]]
				}
			}
		})
	},
	onload:function(frm){
        // console.log(frm.doc.name)
        // console.log((frm.doc.name.search("new-sales-order-1") >= 0))
        if(frm.doc.__islocal){
            frappe.call({
                "method":"shipping_management.shipping_management.sales_order.get_sales_person",
                "args":{
                    "user":frappe.session.user
                },
                callback:function(res){
                    console.log(res)
                    if(res.message){
                       
						frm.set_value("sales_person",res.message)
						frm.refresh_field("sales_person")
    
                    }
                }
    
            })
        }
        
    }
	
});
