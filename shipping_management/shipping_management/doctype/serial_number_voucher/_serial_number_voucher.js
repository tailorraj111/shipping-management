// Copyright (c) 2022, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Serial Number Voucher', {
	refresh: function(frm) {
		cur_frm.fields_dict['work_order'].get_query = function(doc) {
			return {
				filters: {
					"status": 'In Process'
				}
			}
		}
	},
	get_items: function(frm) {
		if(frm.doc.work_order){
			cur_frm.clear_table("items")
			frappe.call({
				method: 'get_items',
				doc: frm.doc,
				callback: function(res){
					console.log(res)
					var lopping = res.message[0]['qty'];
					for (let index = 0; index < lopping; index++) {
						var row = frm.add_child("items");
						row.item_code = res.message[0]['production_item'];
					}
					frm.doc.item_code = res.message[0]['production_item'];
					refresh_field('item_code');
					refresh_field('items');
				}
			});
		}
		else{
			frappe.msgprint("Enter Work Order Please.")
		}
	},
	generate_serial_number_or_batch: function(frm){
		frappe.call({
			method: 'get_serial_no_batch',
			doc: frm.doc,
			callback: function(res){
				console.log(res)
				// var lopping = res.message[0]['qty'];
				// for (let index = 0; index < lopping; index++) {
				// 	var row = frm.add_child("items");
				// 	row.item_code = res.message[0]['production_item'];
				// }
				refresh_field('items');
			}
		});
	}
});
