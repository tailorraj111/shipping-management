// Copyright (c) 2022, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Serial Number Voucher', {
	refresh: function(frm) {

		cur_frm.fields_dict['work_order'].get_query = function(doc) {
			return {
				filters: {
					"status": 'In Process'
				}
			}
		},
		cur_frm.fields_dict['material_transfer'].get_query = function(doc) {
			return {
				filters: {
					"work_order": doc.work_order
				}
			}
		}
	},
	
	get_items: function(frm) {
		console.log(frm.doc.work_order)
		console.log(frm.doc.depends_on_raw_material)
		console.log(frm.doc.material_transfer)
		if(frm.doc.work_order && !frm.doc.depends_on_raw_material && !frm.doc.material_transfer){
			cur_frm.clear_table("items")
			frappe.call({
				method: 'get_items',
				doc: frm.doc,
				callback: function(res){
					console.log(res)
					var lopping = res.message[0]['qty'];
					for (let index = 0; index < lopping; index++) {
						var row = frm.add_child("items");
						row.item_code = res.message[0]['production_item'];
					}
					
					refresh_field('items');
				}
			});
		}else if(frm.doc.work_order && frm.doc.depends_on_raw_material){
			if(!frm.doc.material_transfer){
				frappe.msgprint("Select Material transfer voucher")
			}else{
				frappe.call({
					method: 'get_items_from_se',
					doc: frm.doc,
					callback: function(res){
						// console.log(res)
						if (res.message) {
							res.message.forEach(element => {
								var row = frm.add_child("items");
								row.item_code = frm.doc.item_code;
								row.raw_material_sn = element.serial_no;
							});
							
						}
						refresh_field('items');
					}
				});
			}

		}
		else{
			frappe.msgprint("Enter Work Order Please.")
		}
	},
	generate_serial_number_or_batch: function(frm){
		frappe.call({
			method: 'get_serial_no_batch',
			doc: frm.doc,
			callback: function(res){
				console.log(res)
				refresh_field('items');
			}
		});
	},
	gen_sn: function(frm){
		frappe.call({
			method: 'get_serial_no',
			doc: frm.doc,
			callback: function(res){
				console.log(res)
				refresh_field('items');
			}
		});
	}
});
