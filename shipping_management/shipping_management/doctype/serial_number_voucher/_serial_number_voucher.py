# Copyright (c) 2022, Raj Tailor and contributors
# For license information, please see license.txt

# from dataclasses import FrozenInstanceError
# from itertools import count
# from re import I
import frappe
from frappe.model.document import Document

class SerialNumberVoucher(Document):
	@frappe.whitelist()
	def get_items(self):
		product = frappe.db.sql("""
		select production_item, qty from `tabWork Order`where name = %s
		""", (self.work_order), as_dict = True)
		return product

	@frappe.whitelist()
	def get_serial_no_batch(self):
		batch = generate_batch_basedon_currdate()
		serial_nos = generate_serial_number(len(self.items),batch)
		
		for item in self.items:

			item.serial_number = serial_nos[item.idx-1]

			item.batch_number = batch

			item.barcode = serial_nos[item.idx-1]
			
		return 1
	
	def after_insert(self):
		for item in self.items:
			get_serial = frappe.get_doc({
			'doctype': 'Serial No',
			'serial_no': item.serial_number,
			'item_code': self.items[0].item_code
			# 'batch_no': item.batch_number
			})

			get_serial.save()

		last_serial_number = frappe.db.get_value("Custom Batch Setting",None,"last_generated_serial_number")
		frappe.db.set_value("Custom Batch Setting",None,"last_generated_serial_number",int(last_serial_number)+len(self.items))
		frappe.msgprint("Batch Number and Serials Created Successfully!")


def generate_batch_basedon_currdate():
	date = frappe.db.get_value("Custom Batch Setting",None,"date")
	batch = frappe.db.get_value("Custom Batch Setting",None,"today_batch")
	if date == frappe.utils.today() and batch:
		return batch
	else:
		from datetime import datetime
		currentMonth = datetime.now().strftime("%m")
		currentYear = datetime.now().strftime("%y")
		currentdate = datetime.now().strftime("%d")
		currmonth_alpha = chr(int(currentMonth)+64)
		currentYear_alpha = chr(int(currentYear)+64)
		new_batch = str(currentdate)+str(currmonth_alpha)+str(currentYear_alpha)
		batch = frappe.db.set_value("Custom Batch Setting",None,"today_batch",new_batch)
		return new_batch

def generate_serial_number(total_row,batch):
	last_serial_number = frappe.db.get_value("Custom Batch Setting",None,"last_generated_serial_number")
	serial_nos = []
	if last_serial_number:
		start = int(last_serial_number) + 1
	else:
		start = 1
	for i in range(total_row):
		len_ser = len(str(start + i))
		serial_num = ""
		while len_ser < 6:
			serial_num = serial_num + '0'
			len_ser = len_ser + 1
		serial_num = serial_num + str(start + i)
		serial_nos.append(str(batch)+str(serial_num))
	return serial_nos
