# Copyright (c) 2022, Raj Tailor and contributors
# For license information, please see license.txt

# from dataclasses import FrozenInstanceError
# from itertools import count
# from re import I
import frappe
from frappe.model.document import Document

class SerialNumberVoucher(Document):
		
	@frappe.whitelist()
	def get_items(self):
		work_order_status = frappe.db.get_value("Work Order",self.work_order,"status")
		if work_order_status != "In Process":
			frappe.throw("Serial number is required for Work Orders in 'In Process' status")

		serialize_item = frappe.db.get_value("Item", self.item_code, "has_serial_no")
		if not serialize_item:
			frappe.throw("Serial numbers can only be generated for serialized items.")

		product = frappe.db.sql("""
		select production_item, qty from `tabWork Order`where name = %s
		""", (self.work_order), as_dict = True)
		return product
	
	@frappe.whitelist()
	def get_items_from_se(self):
		depends_on_rm = frappe.db.get_value("Item",self.item_code,"raw_material")
		
		if not depends_on_rm:
			frappe.throw("Please add raw material link in Item Master")
		# product_with_sn = frappe.db.sql("""
		# select item_code,serial_no,qty from `tabStock Entry Detail` sed where sed.parent = %(se_name)s and sed.item_code=%(item_code)s
		# """,({
		# 	"se_name":self.material_transfer,
		# 	"item_code":depends_on_rm
		# }),as_dict=1)
			
		product_with_sn =  frappe.db.sql("""
		SELECT DISTINCT sbe.serial_no 
		FROM `tabSerial and Batch Bundle` sbb
		INNER JOIN`tabSerial and Batch Entry`sbe on sbb.name = sbe.parent
		WHERE sbb.item_code = %(item_code)s and sbb.voucher_no = %(se_name)s
		""", ({
			"item_code": depends_on_rm,
			"se_name":self.material_transfer,
		}), as_dict=1)

				
		if product_with_sn:	
			return product_with_sn
		else:
			frappe.throw("Serial And Batch Bundle Not Found For This Item: <b>{0}<b> and Voucher: <b>{1}</b>".format(depends_on_rm,self.material_transfer))


	@frappe.whitelist()
	def get_serial_no_batch(self):
		batch = generate_batch_basedon_currdate()
		serial_nos = generate_serial_number(len(self.items),batch)
		
		for item in self.items:

			item.serial_number = serial_nos[item.idx-1]

			item.batch_number = batch

			item.barcode = serial_nos[item.idx-1]
			
		return 1
	@frappe.whitelist()
	def get_serial_no(self):
		raw_material_item = frappe.db.get_value("Item",self.item_code,"raw_material")
		for item in self.items:
			item.item_code = self.item_code
			item.raw_material_item = raw_material_item
			item.serial_number =  item.raw_material_sn+"T"
			
		return 1
	
	def after_insert(self):
		for item in self.items:
			get_serial = frappe.get_doc({
			'doctype': 'Serial No',
			'serial_no': item.serial_number,
			'item_code': self.items[0].item_code,
			'status': 'Inactive',
			'work_order':self.work_order,
			# 'batch_no': item.batch_number
			})

			get_serial.save()
		if not self.depends_on_raw_material:
			last_serial_number = frappe.db.get_value("Custom Batch Setting",None,"last_generated_serial_number")
			frappe.db.set_value("Custom Batch Setting",None,"last_generated_serial_number",int(last_serial_number)+len(self.items))
		frappe.msgprint("Batch Number and Serials Created Successfully!")

	def validate(self):
		if not self.work_order:
			frappe.throw("Work Order is Manadatory")
		
		# for item in self.items:
		# 	if frappe.db.exists("Serial Number Voucher Item" , {"serial_number" : item.serial_number}):
		# 		frappe.throw("Serial Number already used, Please Re-generate serial numbers.")

def generate_batch_basedon_currdate():
	date = frappe.db.get_value("Custom Batch Setting",None,"date")
	batch = frappe.db.get_value("Custom Batch Setting",None,"today_batch")
	if date == frappe.utils.today() and batch:
		return batch
	else:
		from datetime import datetime
		currentMonth = datetime.now().strftime("%m")
		currentYear = datetime.now().strftime("%y")
		currentdate = datetime.now().strftime("%d")
		currmonth_alpha = chr(int(currentMonth)+64)
		currentYear_alpha = chr(int(currentYear)+64)
		new_batch = str(currentdate)+str(currmonth_alpha)+str(currentYear_alpha)
		batch = frappe.db.set_value("Custom Batch Setting",None,"today_batch",new_batch)
		return new_batch

def generate_serial_number(total_row,batch):
	last_serial_number = frappe.db.get_value("Custom Batch Setting",None,"last_generated_serial_number")
	serial_nos = []
	if last_serial_number:
		start = int(last_serial_number) + 1
	else:
		start = 1
	for i in range(total_row):
		len_ser = len(str(start + i))
		serial_num = ""
		while len_ser < 6:
			serial_num = serial_num + '0'
			len_ser = len_ser + 1
		serial_num = serial_num + str(start + i)
		serial_nos.append(str(batch)+str(serial_num))
	return serial_nos