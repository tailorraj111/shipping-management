# Copyright (c) 2024, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe import _
import time
from rq.job import Job
from frappe.utils.background_jobs import get_job



class EnqueueDocumentSubmission(Document):
	pass	

@frappe.whitelist()
def submit_vouchers(enqueue_doc,doctype, voucher_no):
	job = frappe.enqueue(
		"shipping_management.shipping_management.doctype.enqueue_document_submission.enqueue_document_submission.submit_voucher", 
		queue='long', 
		doctype=doctype, 
		voucher_no=voucher_no,
		enqueue_doc=enqueue_doc
	)
	frappe.db.set_value("Enqueue Document Submission",enqueue_doc,"status","Enqueued")
	job_status = None
	while job_status not in ['finished', 'failed']:
		job.refresh()
		job_status = job.get_status()
		time.sleep(1) 

	if job_status == 'finished':
		frappe.msgprint(_("Document {0} {1} submitted successfully.").format(doctype, voucher_no))
		frappe.db.set_value("Enqueue Document Submission",enqueue_doc,"status","Completed")
	elif job_status == 'failed':
		frappe.msgprint(_("Document {0} {1} is already submitted.").format(doctype, voucher_no))
	else:
		frappe.msgprint("Unknown job status.")


@frappe.whitelist()
def submit_voucher(enqueue_doc,doctype, voucher_no):
	try:
		frappe.db.set_value("Enqueue Document Submission",enqueue_doc,"status","Running")
		submit_doc = frappe.get_doc(doctype, voucher_no)
		if submit_doc.docstatus == 1:
			raise Exception(_("Document {0} {1} is already submitted.").format(doctype, voucher_no))

		submit_doc.submit()
		frappe.db.commit()
	except Exception as e:
		frappe.db.set_value("Enqueue Document Submission",enqueue_doc,"error_log",e)
		frappe.db.set_value("Enqueue Document Submission",enqueue_doc,"status","Failed")		