// Copyright (c) 2024, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on("Enqueue Document Submission", {
	refresh(frm) {
        frm.set_query('voucher_no', () => {
            return {
                filters: {
                    docstatus: 0
                }
            }
        })
	},
    submit_voucher: function(frm){
        frappe.call({
            method: "shipping_management.shipping_management.doctype.enqueue_document_submission.enqueue_document_submission.submit_vouchers",
            args:{
                'doctype':frm.doc.voucher_type,
                'voucher_no':frm.doc.voucher_no,
                'enqueue_doc':frm.doc.name
            },
            callback: function(response) {
                // Handle response here
            }
                
        });
        
    }
});
