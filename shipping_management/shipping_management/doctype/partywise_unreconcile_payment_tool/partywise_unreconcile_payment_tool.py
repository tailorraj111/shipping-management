# Copyright (c) 2024, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class PartywiseUnreconcilePaymentTool(Document):
	pass


@frappe.whitelist()
def get_partywise_allocations_from_payment(party, from_date, to_date, voucher_type):
	try:
		
		unreconcile_entries = frappe.db.sql("""
			SELECT pe.name 
			FROM `tabPayment Entry` pe 
			WHERE (SELECT COUNT(*) FROM `tabPayment Entry Reference` per WHERE per.parent = pe.name) > 0
				AND pe.docstatus < 2 AND pe.party = %s 
				AND pe.posting_date BETWEEN %s AND %s
		""", (party, from_date, to_date), as_dict=True)
		
		voucher_created = False

		if unreconcile_entries:
			for ue in unreconcile_entries:
				if not frappe.db.exists("Unreconcile Payment", {"voucher_no": ue.name, "voucher_type": voucher_type}):
					un_doc = frappe.new_doc("Unreconcile Payment")
					un_doc.company = frappe.db.get_value("Global Defaults", None, "default_company")
					un_doc.voucher_type = voucher_type
					un_doc.voucher_no = ue.name
					un_doc.add_references()
					un_doc.insert(ignore_permissions=True)
				else:
					frappe.msgprint("Unreconcile Payment Entries Already Generated for this Customer.")
			
		if voucher_created:
			frappe.msgprint("Unreconcile Payment Voucher Created")
		else:
			frappe.msgprint("No payment entries found matching the specified criteria.")
		
	
	except frappe.ValidationError as e:
		frappe.throw(str(e))
	
	except Exception as e:
		frappe.log_error("Unreconcile Payment Voucher Creation Failed", message=str(e))
		frappe.msgprint("Error occurred while creating Unreconcile Payment Voucher.")