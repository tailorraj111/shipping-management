// Copyright (c) 2024, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on("Partywise Unreconcile Payment Tool", {
	refresh(frm) {
        frm.set_query("voucher_type", function() {
			return {
				filters: {
					name: ["in", ["Payment Entry"]]
				}
			}
		});
        frm.set_query("party_type", function() {
			return {
				filters: {
					name: ["in", ["Customer"]]
				}
			}
		});

        frm.add_custom_button("Create Unreconcile Entries", function(){
            
            frappe.call({
                method: "shipping_management.shipping_management.doctype.partywise_unreconcile_payment_tool.partywise_unreconcile_payment_tool.get_partywise_allocations_from_payment",
                args:{
                    "party":frm.doc.party,
                    "from_date":frm.doc.from_date,
                    "to_date":frm.doc.to_date,
                    "voucher_type":frm.doc.voucher_type
                },
                callback: function(r) {
                    console.log(r.message)
                    if (r.message) {
                        frappe.msgprint("Your Unreconcile Payment Voucher Is Created")
                    }
                }
            })
        });
	},
    from_date:function(frm){
        if(frm.doc.from_date > frm.doc.to_date){
            frappe.throw("From Date must be before To Date")
        }
    },
    to_date:function(frm){
        if(frm.doc.from_date > frm.doc.to_date){
            frappe.throw("From Date must be before To Date")
        }
    }
});
