# Copyright (c) 2021, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc

class CertificateRequest(Document):
	pass

@frappe.whitelist()
def make_certificate(source_name, target_doc=None, ignore_permissions=False):
	def postprocess(source, target):
		pass

	def set_missing_values(source, target):
		pass

	def update_item(source, target, source_parent):
		pass

	doclist = get_mapped_doc("Certificate Request", source_name, {
		"Certificate Request": {
			"doctype": "Certificate Issue",
			"field_map": {
				
			}
		}
	}, target_doc, postprocess, ignore_permissions=ignore_permissions)

	return doclist
