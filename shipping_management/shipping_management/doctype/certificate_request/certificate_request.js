// Copyright (c) 2021, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Certificate Request', {
	refresh: function(frm) {
		frm.add_custom_button(__('Issue Certificate'), (frm) => {
			frappe.model.open_mapped_doc({
				method: "shipping_management.shipping_management.doctype.certificate_request.certificate_request.make_certificate",
				frm: cur_frm
			})
		})
	}
});
