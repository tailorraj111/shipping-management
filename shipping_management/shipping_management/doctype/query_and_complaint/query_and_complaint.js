// Copyright (c) 2020, Raj Tailor and contributors
// For license information, please see license.txt

frappe.ui.form.on('Query and Complaint', {
	refresh(frm) {
		// your code here
		if(!frm.doc.raised_by){
		    var logged_user = frappe.session.user;
		    if(logged_user == "Administrator"){
		    frappe.db.get_value("User",{"first_name":logged_user},["email"]).then((res) => {
	       // console.log(res.message.email);
	     frm.set_value("raised_by",res.message.email)
	    
		    })
		}
		    frm.set_value("raised_by",frappe.session.user)
		}
		//==========================
    $("[data-fieldname=call_description]").mouseover(function(){
        console.log('mouse over done')
       $("[data-fieldname=call_description]" ).css("height", "auto");
       $(".rows .grid-row .data-row .ellipsis").css('white-space','break-spaces');
    //   alert("onload");
    })

    $("[data-fieldname=call_description]").mouseout(function(){
        $("[data-fieldname=call_description]" ).css("height", "38px");
    })

	},
	before_save:function(frm){
	    var logged_user = frappe.session.user;
		    if(logged_user == "Administrator"){
		    frappe.db.get_value("User",{"first_name":logged_user},["email"]).then((res) => {
	       // console.log(res.message.email);
	     frm.set_value("raised_by",res.message.email)
	    
		    })
		} else {
	    frm.set_value("raised_by",frappe.session.user)
		}
	}
})
frappe.ui.form.on('Call Log Details', {
	date:function(frm, cdt, cdn){
	     var row = locals[cdt][cdn];
	     
	     	console.log(frappe.session.user);
		var logged_user = frappe.session.user;
		var logged_user_email = "";
		if(logged_user == "Administrator"){
		    frappe.db.get_value("User",{"first_name":logged_user},["email"]).then((res) => {
	       // console.log(res.message.email);

	     frappe.db.get_value("User",{"email":res.message.email},["full_name"]).then((res1) => {
	        frappe.model.set_value(cdt, cdn,"by_user",res1.message.full_name);
	       // console.log(res1);
	    })
	    
		    })
		} else{
		     frappe.db.get_value("User",{"email":frappe.session.user},["full_name"]).then((res2) => {
	        frappe.model.set_value(cdt, cdn,"by_user",res2.message.full_name);
	       
	    })
		}
	     
	     
	     
	    // frappe.model.set_value(cdt, cdn,"by_user",frappe.user.full_name(frappe.session.user));
	   
	}
})
