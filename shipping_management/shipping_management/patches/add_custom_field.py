import frappe

def execute():
    add_field_in_item_master()
    add_field_in_issue_doctype()

def add_field_in_item_master():
    if not frappe.db.exists("Custom Field", {"dt": "Item", "fieldname": "customer_item_code"}):
        # Create the custom field
        custom_field = frappe.get_doc({
            "doctype": "Custom Field",
            "dt": "Item",
            "fieldname": "customer_item_code",
            "label": "Customer Item Code",
            "fieldtype": "Data",
            "insert_after": "reference_code" 
        })
        custom_field.insert(ignore_permissions=True)
        print("Field 'customer_item_code' added successfully to Item doctype.")
    else:
        print("Field 'customer_item_code' already exists in Item doctype.")
    

    if not frappe.db.exists("Custom Field", {"dt": "Item", "fieldname": "duty_status"}):
        # Create the custom field
        custom_field = frappe.get_doc({
            "doctype": "Custom Field",
            "dt": "Item",
            "fieldname": "duty_status",
            "label": "Duty Status",
            "fieldtype": "Select",
            "options":"\nIGCR\nLocal\nDuty Paid\nZero Duty",
            "insert_after": "zero_duty" 
        })
        custom_field.insert(ignore_permissions=True)
        print("Field 'duty_status' added successfully to Item doctype.")
    else:
        print("Field 'duty_status' already exists in Item doctype.")

def add_field_in_issue_doctype():
    if not frappe.db.exists("Custom Field", {"dt": "Issue", "fieldname": "custom_issue_raised_by"}):
        # Create the custom field
        custom_field = frappe.get_doc({
            "doctype": "Custom Field",
            "dt": "Issue",
            "fieldname": "custom_issue_raised_by",
            "label": "Issue Raised By",
            "fieldtype": "Data",
            "insert_after": "customer_input_name" 
        })
        custom_field.insert(ignore_permissions=True)
        print("Field 'customer_item_code' added successfully to Item doctype.")
    else:
        print("Field 'customer_item_code' already exists in Item doctype.")