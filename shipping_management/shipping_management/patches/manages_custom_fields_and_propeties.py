import frappe

def execute():
    remove_property_setter()
    add_mendatory_permissions()

def remove_custom_field():
    doctypes = ["Quotaion","Sales Order","Sales Invoice","Delivery Note"]
    fieldname = "shipping_method"
    try:
        for doctype in doctypes:
            if frappe.db.exists("Custom Field", {"fieldname": fieldname, "dt": doctype}):
                frappe.db.sql("ALTER TABLE `tab{}` DROP COLUMN `{}`;".format(doctype, fieldname))
                frappe.db.delete(
                    "Custom Field",
                    {
                        "fieldname": ("in", fieldname),
                        "dt": ("in", doctype),
                    },
                )
            
        print("Custom field '{}' removed successfully from all specified doctypes".format(fieldname))
    except Exception as e:
        print("Error occurred while removing custom field: ", str(e))


def remove_property_setter():
    property_names = ["Customer-tax_category-reqd"]
    try:
        for property in property_names:
            if frappe.db.exists("Property Setter",{"name":property}):
                frappe.db.delete(
                    "Property Setter",
                    {
                        "name": ("=", property),
                    },
                )
            frappe.db.commit()
            print("Property setter '{}' removed successfully".format(property))
    except Exception as e:
        print("Error occurred while removing property setter: ", str(e))

def add_property_setter():
    print("Adding property setter")
    field_propeties = [
        {
            "doctype": "Customer",
            "fieldname": "tax_id",
            "property": "read_only",
            "value": 1
        },
        {
            "doctype": "Quotaion",
            "fieldname": "additional_discount_percentage",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Quotaion",
            "fieldname": "custom_discount_amount",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order",
            "fieldname": "additional_discount_percentage",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order",
            "fieldname": "custom_discount_amount",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice",
            "fieldname": "additional_discount_percentage",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice",
            "fieldname": "custom_discount_amount",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Delivery Note",
            "fieldname": "additional_discount_percentage",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Delivery Note",
            "fieldname": "custom_discount_amount",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order Item",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order Item",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Order Item",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice Item",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice Item",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Sales Invoice Item",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Order",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Order",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Order",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Order Item",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Order Item",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Order Item",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Receipt",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Receipt",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Receipt Item",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Receipt Item",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Receipt Item",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Invoice",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Invoice",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Invoice",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Invoice Item",
            "fieldname": "project",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Invoice Item",
            "fieldname": "division",
            "property": "hidden",
            "value": 1
        },
        {
            "doctype": "Purchase Invoice Item",
            "fieldname": "department",
            "property": "hidden",
            "value": 1
        },

    ]
    try:
        for prop in field_propeties:
            print("Adding property setter for '{}'".format(prop["fieldname"]))
            if not frappe.db.exists("Property Setter",{"property":prop["property"],"doc_type":prop["doctype"],"field_name":prop["fieldname"]}):
                property_setter = frappe.new_doc("Property Setter")
                property_setter.property = prop["property"]
                property_setter.doctype_or_field = "DocField"
                property_setter.doc_type = prop["doctype"]
                property_setter.field_name = prop["fieldname"]
                property_setter.value = prop["value"]
                property_setter.insert()
                frappe.db.commit()
                print("Property setter '{}' added successfully".format(prop["property"]))
            else:
                print("Property setter '{}' already exists".format(prop["property"]))
    except Exception as e:
        print("Error occurred while adding property setter: ", str(e))


    
                
def add_mendatory_permissions():
    role_assignments = [{
        "doctype": "Accounts Settings",
        "role": "Sales Executive",
        "permission_level": 0,
        "prmissions": ["read"]
    },
    {
        "doctype": "Stock Settings",
        "role": "Sales Executive",
        "permission_level": 0,
        "prmissions": ["read"]
    },
    ]
    try:
        for role in role_assignments:
            if not frappe.db.exists("Custom DocPerm",{"role":role["role"],"parent":role["doctype"],"permlevel":role["permission_level"]}):
                docperm = frappe.new_doc("Custom DocPerm")
                docperm.role = role["role"]
                docperm.parent = role["doctype"]
                docperm.permlevel = role["permission_level"]
                
                docperm.insert()
                frappe.db.commit()
                print("Permissions for '{}' added successfully".format(role["role"]))
            else:
                print("Permissions for '{}' already exists".format(role["role"]))
    except Exception as e:
        print("Error occurred while adding permissions: ", str(e))




