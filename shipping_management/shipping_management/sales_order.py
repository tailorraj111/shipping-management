from __future__ import unicode_literals
import json
import frappe, erpnext
from frappe import _, scrub
from frappe.utils import cint, flt, round_based_on_smallest_currency_fraction
from erpnext.controllers.accounts_controller import validate_conversion_rate, \
	validate_taxes_and_charges, validate_inclusive_tax
from erpnext.selling.doctype.customer.customer import get_credit_limit
from erpnext.stock.utils import get_stock_balance
from frappe.utils import flt, comma_or, nowdate
from shipping_management.override.taxes_and_totals import calculate_taxes_and_totals
from shipping_management.shipping_management.quotation import update_item_total





def on_update(self,method):
	pass

def validate(self,method):
	if self.gst_category != 'Overseas':
		if len(self.taxes) == 0 or self.total_taxes_and_charges == 0.00:
			frappe.throw("Please Add Taxes!!")
	if self.workflow_state in ["Rejected","Approved"] and not self.rejection_reason:
		frappe.throw("Please Add Approved/Rejection Reason for Approve/Reject this Sales Order")
	
	
	
	if self.docstatus == 0:
		set_account_data(self)
	check_oem_form_item(self)

	update_item_total(self)
	calculate_taxes_and_totals(self)
	



@frappe.whitelist()
def oem_form(order_doc):
    order_doc = json.loads(order_doc)
    try:
        get_oem_form = frappe.db.get_value("OEM Form",{"customer_name":order_doc['customer']},"name")
        if get_oem_form:
            return get_oem_form
    except KeyError:
        frappe.throw("Please insert the Customer Name")
def check_oem_form_item(self):
	
	

	order_items = list(filter(lambda x: x.item_brand not in ["Trueview", "OEM" , "Raw Material"] , self.items))

	if len(order_items) > 0:
		for item in order_items:
			if not frappe.db.exists("OEM Form Items", {
				"warner_model_no" : item.item_code
			}):
				frappe.throw(item.item_code + " is not in OEM Form. First Update the OEM Form")
	




@frappe.whitelist()
def set_account_data(self):
	
	credit  = get_credit_limit(self.customer, self.company)
	oustanding = get_customer_outstanding(self.customer, self.company)
	
	previous_out = oustanding[0]
	approved_so = oustanding[1]
	gle_bal = oustanding[2]
	self.credit_limit = credit
	self.outstanding_limit = previous_out
	self.overdue_amount =  float(previous_out) - float(credit) + self.rounded_total
	self.current_outstanding = float(previous_out)
	self.approved_sales_order = approved_so
	self.current_ledger_balance = gle_bal
	if self.overdue_amount > 0:
		self.credit_limit_crossed = 1
	else:
		self.credit_limit_crossed = 0
	

@frappe.whitelist()
def make_payment_entry(self):
	self = json.loads(self)
	payment_doc = frappe.get_doc({
		"doctype":"Payment Entry",
		"posting_date":self["payment_date"],
		"remarks":self["payment_remarks"],
		"payment_type":"Receive",
		"received_amount":float(self["paid_amount"]),
		"mode_of_payment":self["mode_of_payment"],
		"party_type":"Customer",
		"party":self["customer"],
		"paid_amount":float(self["paid_amount"]),
		"paid_from":"Debtors - T",
		"paid_to":"ICICI C/A - 2435 - T",
		"reference_no":"check ref",
		"reference_date":nowdate(),
		
	})
	res = payment_doc.insert()
	payment_doc.submit()
	frappe.db.set_value("Sales Order",self["name"],"payment_entry",res.name)
	return res

@frappe.whitelist()
def refresh_account_data(self):
	if type(self) == str:
		self = json.loads(self)
	credit  = get_credit_limit(self["customer"], self["company"])
	oustanding = get_customer_outstanding(self["customer"], self["company"])
	# frappe.msgprint(str(oustanding))
	previous_out = oustanding[0]
	approved_so = oustanding[1]
	gle_bal= oustanding[2]
	return{
		"credit_limit":credit,
		"outstanding_limit":previous_out,
		"overdue_amount" :  float(previous_out) - float(credit) + self["rounded_total"],
		"current_outstanding" : float(previous_out),
		"approved_sales_order":approved_so,
		"current_ledger_balance":gle_bal

	}

def on_submit(self,method):
	
	customer_state = frappe.db.get_value("Customer",self.customer,"workflow_state")
	if customer_state != "Approved":
		frappe.throw("Your Sales Order can not be Approved as {} is not approved customer.".format(self.customer_name))
	for item in self.items:
		state = frappe.db.get_value("Item",item.item_code,"workflow_state")
		if state != "Approved":
			frappe.throw("Your Sales Order can not be Approved as {} is not approved Item.".format(item.item_code))

@frappe.whitelist()
def get_sales_person(user):
	employee = frappe.db.get_value("Employee",{"user_id":user},"name")
	

	if employee:
		sales_person = frappe.db.get_value("Sales Person",{"employee":employee},"name")
		if sales_person:
			return sales_person


def after_insert(self,method):
	pass


def check_credit_on_insert(self,method):
	
	if not cint(frappe.db.get_value("Customer Credit Limit",{'parent': self.customer, 'parenttype': 'Customer', 'company': self.company},"bypass_credit_limit_check")):
		if check_credit_limit(self.customer, self.company,self.base_grand_total):
			
			self.credit_limit_crossed = 1
			frappe.db.set_value("Sales Order",self.name,"credit_limit_crossed",1)
			frappe.db.commit()

def check_credit_limit(customer, company,so_amt, ignore_outstanding_sales_order=False, extra_amount=0):
	customer_outstanding = get_customer_outstanding(customer, company,so_amt, ignore_outstanding_sales_order)
	if extra_amount > 0:
		customer_outstanding += flt(extra_amount)

	credit_limit = get_credit_limit(customer, company)
	if credit_limit > 0 and flt(customer_outstanding) > credit_limit:
		return 1
	else:
		return 0

@frappe.whitelist()
def get_customer_outstanding(customer, company,so_amt = 0, ignore_outstanding_sales_order=False, cost_center=None):

	cond = ""
	if cost_center:
		lft, rgt = frappe.get_cached_value("Cost Center",
			cost_center, ['lft', 'rgt'])

		cond = """ and cost_center in (select name from `tabCost Center` where
			lft >= {0} and rgt <= {1})""".format(lft, rgt)

	outstanding_based_on_gle = frappe.db.sql("""
		select sum(debit) - sum(credit)
		from `tabGL Entry` where party_type = 'Customer'
		and party = %s and company=%s {0}""".format(cond), (customer, company))

	outstanding_based_on_gle = flt(outstanding_based_on_gle[0][0]) if outstanding_based_on_gle else 0

	
	outstanding_based_on_so = 0.0

	if not ignore_outstanding_sales_order:
		outstanding_based_on_so = frappe.db.sql("""
			select sum(base_grand_total*(100 - per_billed)/100)
			from `tabSales Order`
			where customer=%s and docstatus = 1 and company=%s
			and per_billed < 100 and status != 'Closed'""", (customer, company))

		outstanding_based_on_so = flt(outstanding_based_on_so[0][0]) if outstanding_based_on_so else 0.0

	unmarked_delivery_note_items = frappe.db.sql("""select
			dn_item.name, dn_item.amount, dn.base_net_total, dn.base_grand_total
		from `tabDelivery Note` dn, `tabDelivery Note Item` dn_item
		where
			dn.name = dn_item.parent
			and dn.customer=%s and dn.company=%s
			and dn.docstatus = 1 and dn.status not in ('Closed', 'Stopped')
			and ifnull(dn_item.against_sales_order, '') = ''
			and ifnull(dn_item.against_sales_invoice, '') = ''
		""", (customer, company), as_dict=True)

	outstanding_based_on_dn = 0.0

	for dn_item in unmarked_delivery_note_items:
		si_amount = frappe.db.sql("""select sum(amount)
			from `tabSales Invoice Item`
			where dn_detail = %s and docstatus = 1""", dn_item.name)[0][0]

		if flt(dn_item.amount) > flt(si_amount) and dn_item.base_net_total:
			outstanding_based_on_dn += ((flt(dn_item.amount) - flt(si_amount)) \
				/ dn_item.base_net_total) * dn_item.base_grand_total

	return ((outstanding_based_on_gle + outstanding_based_on_so + outstanding_based_on_dn + so_amt),outstanding_based_on_so,outstanding_based_on_gle)



@frappe.whitelist()
def check_stock(order_doc):
	over_stock = 0
	out_of_stock = []
	
	order_doc = json.loads(order_doc)

	grouped_data = order_doc['items']
	unique_data = []

	for item in grouped_data:
		if not unique_data:
			unique_data.append({
				"item_code": item['item_code'],
				"warehouse":item['warehouse'],
				"qty": item['qty'],
				"idx":item['idx']
				})
		else:
			flag = 0
			for i in unique_data:
				if item['item_code'] not in i['item_code']:
					flag = 1
					continue
				else:
					i['qty'] += item['qty']
					flag = 0
					break
			
			if flag == 1:
				unique_data.append({
					"item_code": item['item_code'],
					"warehouse":item['warehouse'],
					"qty": item['qty'],
					"idx":item['idx']
					})
					
		

	
	for item in unique_data:
		
		
		warehouse_stock = get_stock_balance(item['item_code'],item['warehouse'])
		maintain_stock = frappe.db.get_value("Item",item['item_code'],"is_stock_item")
		
		if item['qty'] > warehouse_stock and maintain_stock:
			over_stock = 1
			out_of_stock.append({
				"item_code":item['item_code'],
				"warehouse":item['warehouse'],
				"qty":item['qty'],
				"warehouse_qty":warehouse_stock,
				"row":item['idx']
			})
	if over_stock == 1:
		
		table = """<h3>Stock Status</h3><br><table class="table-bordered"><tr>
		<th>Row No.</th>
		<th>Item Code</th>
		<th>Warehouse</th>
		<th>Warehouse Qty</th>
		<th>Ordered Qty</th>
		<th>Short Qty</th>
		</tr>"""
		for item in out_of_stock:
			table = table + """<tr>
			<td>{0}</td>
			<td>{1}</td>
			<td>{2}</td>
			<td>{3}</td>
			<td>{4}</td>
			<td>{5}</td>
			</tr>""".format(str(item['row']),str(item['item_code']),str(item['warehouse']),str(item['warehouse_qty']),str(item['qty']),str(float(item['warehouse_qty'])-float(item['qty'])))
		table = table + """</table>"""
		frappe.msgprint(table)
	else:
		frappe.msgprint("Stock is available for all Item!")
		


@frappe.whitelist()
def generate_picklist(doc):
	doc = json.loads(doc)
	# frappe.msgprint(str(doc["items"]))
	picking_list = []
	
	for item in doc["items"]:
		frappe.msgprint(str(item["item_code"]))
		if not any(d['warehouse'] == item["warehouse"] for d in picking_list):
			picking_list.append({
				"warehouse":item["warehouse"],
				"location":[
					{"item_code":item["item_code"],
					"stock_qty":item["qty"],
					"qty":item["qty"],
					"warehouse":item["warehouse"],
					"sales_order":doc["name"],
					"sales_order_item":item["name"]}
				]
			})
		else:
			for pick in picking_list:
				if pick["warehouse"] == item["warehouse"]:
					pick["location"].append({
					"item_code":item["item_code"],
					"stock_qty":item["qty"],
					"qty":item["qty"],
					"warehouse":item["warehouse"],
					"sales_order":doc["name"],
					"sales_order_item":item["name"]
					})
	frappe.msgprint(str(picking_list))
	
	for item in picking_list:
		object = frappe.get_doc({
			"doctype":"Pick List",
			"warehouse":item["warehouse"],
			"purpose":"Delivery",
			"customer":doc["customer"],
			"company":doc["company"],
			"locations":item["location"]
		})
		# object.flags.ignore_permissions = True
		frappe.msgprint(str(object))
		picking = object.insert()
		frappe.msgprint("Picking List Created "+str(picking.name))
	