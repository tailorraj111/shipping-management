import frappe
from frappe import _
from frappe.utils.dashboard import cache_source


@frappe.whitelist()
@cache_source
def get(
    chart_name=None,
    chart=None,
    no_cache=None,
    filters=None,
    from_date=None,
    to_date=None,
    timespan=None,
    time_interval=None,
    heatmap_year=None,
):
    labels, datapoints = [], []
    if chart_name:
        chart = frappe.get_doc("Dashboard Chart", chart_name)
    else:
        chart = frappe._dict(frappe.parse_json(chart))
    # frappe.throw(str(chart))
    time_interval = chart.time_interval
    # frappe.throw(str(time_interval))
    filters = frappe.parse_json(filters)

    sales_persons = frappe.get_list(
        "Sales Invoice",
        fields=["sales_person", "sum(grand_total) as total_sales"],
        filters={
            "docstatus": 1,  # Only fetch approved invoices
            "posting_date": ["between", (filters.get("from_date"), filters.get("to_date"))],
        },
        group_by="sales_person",
    )
    
    # frappe.throw(str(time_interval))
    if not sales_persons:
        return []

    for sales_person in sales_persons:
        labels.append(_(sales_person.get("sales_person")))
        datapoints.append(sales_person.get("total_sales"))

    return {
        "labels": labels,
        "datasets": [{"name": _("Total Sales"), "values": datapoints}],
        "type": "bar",
    }
