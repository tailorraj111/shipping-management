frappe.provide("frappe.dashboards.chart_sources");

frappe.dashboards.chart_sources["Daily Sales By Sales Person"] = {
    method:"shipping_management.shipping_management.dashboard_chart_source.daily_sales_by_sales_person.daily_sales_by_sales_person.get",
	filters: [
		{
			fieldname: "from_date",
			label: __("From Date"),
			fieldtype: "Date",
			reqd: 1,
		},
		{
			fieldname: "to_date",
			label: __("To Date"),
			fieldtype: "Date",
			reqd: 1,
		},
	],
};