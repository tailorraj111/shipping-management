import frappe
from frappe import _
from frappe.utils import flt, cint
# from console import console

import erpnext
from erpnext.accounts.utils import (
	update_gl_entries_after,
)
from erpnext.stock.stock_ledger import repost_future_sle




@frappe.whitelist()
def get_data(filters):
    # console("here").log()

    filters = frappe._dict(filters)
    data = get_stock_ledger_entries(filters)
    itewise_balance_qty = {}

    for row in data:
        key = (row.item_code, row.warehouse)
        itewise_balance_qty.setdefault(key, []).append(row)

    res = validate_data(itewise_balance_qty)
    return res


def validate_data(itewise_balance_qty):
    res = []
    for key, data in itewise_balance_qty.items():
        row = get_incorrect_data(data)
        if row:
            res.append(row)

    return res


def get_incorrect_data(data):
    balance_qty = 0.0
    for row in data:
        balance_qty += row.actual_qty
        if row.voucher_type == "Stock Reconciliation" and not row.batch_no:
            balance_qty = flt(row.qty_after_transaction)

        row.expected_balance_qty = balance_qty
        if abs(flt(row.expected_balance_qty) - flt(row.qty_after_transaction)) > 0.5:
            row.differnce = abs(
                flt(row.expected_balance_qty) - flt(row.qty_after_transaction)
            )
            return row


def get_stock_ledger_entries(report_filters):
    filters = {"is_cancelled": 0}
    fields = [
        "name",
        "voucher_type",
        "voucher_no",
        "item_code",
        "actual_qty",
        "posting_date",
        "posting_time",
        "company",
        "warehouse",
        "qty_after_transaction",
        "batch_no",
    ]

    for field in ["warehouse", "item_code", "company"]:
        if report_filters.get(field):
            filters[field] = report_filters.get(field)

    return frappe.get_all(
        "Stock Ledger Entry",
        fields=fields,
        filters=filters,
        order_by="timestamp(posting_date, posting_time) asc, creation asc",
    )


@frappe.whitelist()
def clear_all_reposting():
    
    repsting_list = frappe.db.get_list('Repost Item Valuation', filters={"status": "Queued"})
    for doc in repsting_list:
        reposting_doc = frappe.get_doc("Repost Item Valuation",doc['name'])
        reposting_doc.allow_negative_stock = 1
        doc = reposting_doc
        try:
            repost_sl_entries(doc)
            repost_gl_entries(doc)
            frappe.db.set_value('Repost Item Valuation', doc.name, 'status', 'Completed')
            frappe.db.commit()
            print("reposting "+str(doc.name)+" clear")
        except Exception as e:
            error = frappe.get_doc({
                "doctype":"Error Log",
                "error":str(e) + str(doc.voucher_type) + str(doc.voucher_no)
            })
            error.insert(ignore_permissions=True)
            
    



def process_incorrect_balance_qty():
    # frappe.publish_realtime('msgprint', 'Starting long job...')
    
    data = frappe.get_last_doc('Repost Item Valuation', filters={"status": "Queued"})
    # frappe.publish_realtime('msgprint', 'Starting long job...')
    from frappe.utils import nowdate, now_datetime
    frappe.db.set_value('Stock Check', 'Stock Check', 'last_reposted', now_datetime())
    data.allow_negative_stock = 1
    
    doc = data
    repost_sl_entries(doc)
    repost_gl_entries(doc)
    frappe.db.set_value('Repost Item Valuation', doc.name, 'status', 'Completed')
    frappe.db.commit()
    
@frappe.whitelist()
def repost_sl_entries(doc):
	if doc.based_on == 'Transaction':
		repost_future_sle(doc=doc, voucher_type=doc.voucher_type, voucher_no=doc.voucher_no,
			allow_negative_stock=doc.allow_negative_stock, via_landed_cost_voucher=doc.via_landed_cost_voucher)
	else:
		repost_future_sle(args=[frappe._dict({
			"item_code": doc.item_code,
			"warehouse": doc.warehouse,
			"posting_date": doc.posting_date,
			"posting_time": doc.posting_time
		})], allow_negative_stock=doc.allow_negative_stock, via_landed_cost_voucher=doc.via_landed_cost_voucher)

@frappe.whitelist()
def repost_gl_entries(doc):
	if not cint(erpnext.is_perpetual_inventory_enabled(doc.company)):
		return

	if doc.based_on == 'Transaction':
		ref_doc = frappe.get_doc(doc.voucher_type, doc.voucher_no)
		items, warehouses = ref_doc.get_items_and_warehouses()
	else:
		items = [doc.item_code]
		warehouses = [doc.warehouse]

	update_gl_entries_after(doc.posting_date, doc.posting_time,
		warehouses, items, company=doc.company)
	# console("acc entry reposted").log()


def process_incorrect_balance_qty_on_click():
    frappe.db.set_value("Stock Check",None,"last_reposted",frappe.utils.now())
    # console("process entry").log()
    data = get_data({})
    # console(data).log()

    if len(data) > 0:
        for rec in data:
            try:
                # console(rec).log()
                doc = frappe.new_doc("Repost Item Valuation")
                doc.based_on = "Transaction"
                doc.voucher_type = rec.voucher_type
                doc.voucher_no = rec.voucher_no
                doc.posting_date = rec.posting_date
                doc.posting_time = rec.posting_time
                doc.company = rec.company
                doc.warehouse = rec.warehouse
                doc.allow_negative_stock = 1
                doc.docstatus = 1
                doc.correction_entry = 1
                doc.insert(ignore_permissions=True)
                frappe.db.commit()
            except Exception as e:
                error = frappe.get_doc({
                    "doctype":"Error Log",
                    "error":str(e) + str(rec.voucher_type) + str(rec.voucher_no)
                })
                error.insert(ignore_permissions=True)