from __future__ import unicode_literals
import json
import frappe, erpnext
from frappe import _, scrub
from frappe.utils import cint, flt, round_based_on_smallest_currency_fraction
from erpnext.controllers.accounts_controller import validate_conversion_rate, \
	validate_taxes_and_charges, validate_inclusive_tax
from erpnext.stock.utils import get_stock_balance
from shipping_management.override.taxes_and_totals import calculate_taxes_and_totals
from shipping_management.shipping_management.quotation import update_item_total


def validate(self,method):

	if self.gst_category != 'Overseas':
		if len(self.taxes) == 0 or self.total_taxes_and_charges == 0.00:
			frappe.throw("Please Add Taxes!!")
	
	if self.update_stock:
		check_stock(self)
	
	check_zero_amt(self)
	validate_item_price(self)

	update_item_total(self)
	calculate_taxes_and_totals(self)


def validate_item_price(self):
	
	values = {'SalesOrder': self.items[0].sales_order}
	data = frappe.db.sql("""
		SELECT
			si.name,
			si.item_name,
			si.rate
		FROM `tabSales Order Item` si
			INNER JOIN `tabSales Order` s
			ON si.parent = s.name
		WHERE s.name = %(SalesOrder)s
		""", values=values, as_dict=1)

	
	for item in self.items:
		for i in data:
			if i.name == item.so_detail:
				if i.rate == item.rate:
					break
				else:
					frappe.throw('The rate of ' + i.item_name + 'in sales order and sales invoice do not match.')



def check_stock(self):
	over_stock = 0
	out_of_stock = []
	for item in self.items:
		
		allow_negative = frappe.db.get_value("Warehouse",item.warehouse,"allow_negative_stock")
		if not allow_negative:
			warehouse_stock = get_stock_balance(item.item_code,item.warehouse)
			maintain_stock = frappe.db.get_value("Item",item.item_code,"is_stock_item")
			# frappe.msgprint(str(warehouse_stock)+" "+str(item.qty))
			if item.qty > warehouse_stock and maintain_stock:
				over_stock = 1
				out_of_stock.append({
					"item_code":item.item_code,
					"warehouse":item.warehouse,
					"qty":item.qty,
					"warehouse_qty":warehouse_stock,
					"row":item.idx
				})
	if over_stock == 1:
		# frappe.throw(str(out_of_stock))
		table = """<h3>Negative Stock Alert</h3><br><table class="table-bordered"><tr>
		<th>Row No.</th>
		<th>Item Code</th>
		<th>Warehouse</th>
		<th>Warehouse Qty</th>
		<th>Ordered Qty</th>
		<th>Short Qty</th>
		</tr>"""
		for item in out_of_stock:
			table = table + """<tr>
			<td>{0}</td>
			<td>{1}</td>
			<td>{2}</td>
			<td>{3}</td>
			<td>{4}</td>
			<td>{5}</td>
			</tr>""".format(str(item['row']),str(item['item_code']),str(item['warehouse']),str(item['warehouse_qty']),str(item['qty']),str(float(item['warehouse_qty'])-float(item['qty'])))
		table = table + """</table>"""
		frappe.throw(table)

	
def check_zero_amt(self):
	for item in self.items:
		if item.rate == 0:
			frappe.throw("The rate of the item, cannot be zero")



        
