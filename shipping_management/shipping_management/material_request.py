import frappe
from erpnext.stock.utils import get_stock_balance
import json

@frappe.whitelist()
def check_stock(order_doc):
	over_stock = 0
	out_of_stock = []
	order_doc = json.loads(order_doc)
	for item in order_doc['items']:
		bom = frappe.db.get_value("BOM",{"item":item['item_code']},"name")
		if bom:
			bom_items = frappe.get_doc("BOM Item",{"parent":bom})
			# frappe.msgprint(str(bom_items.item_code))
			# allow_negative = frappe.db.get_value("Warehouse",item['warehouse'],"allow_negative_stock")
			# if not allow_negative:
			warehouse_stock = get_stock_balance(bom_items.item_code,"Raw Material - T")
			maintain_stock = frappe.db.get_value("Item",bom_items.item_code,"is_stock_item")
			# frappe.msgprint(str(warehouse_stock)+" "+str(item.qty))
			if item['qty'] > warehouse_stock and maintain_stock:
				over_stock = 1
				out_of_stock.append({
					"item_code":bom_items.item_code,
					"warehouse":"Raw Material - T",
					"qty":item['qty'],
					"warehouse_qty":warehouse_stock,
					"row":item['idx'],
					"Item":item['item_code']
				})
	if over_stock == 1:
		# frappe.throw(str(out_of_stock))
		table = """<h3>Stock Status</h3><br><table class="table-bordered"><tr>
		<th>Row No.</th>
		<th>Item</th>
		<th>Raw Material</th>
		<th>Warehouse</th>
		<th>Warehouse Qty</th>
		<th>Required Qty</th>
		<th>Short Qty</th>
		
		</tr>"""
		for item in out_of_stock:
			table = table + """<tr>
			<td>{0}</td>
			<td>{6}</td>
			<td>{1}</td>
			<td>{2}</td>
			<td>{3}</td>
			<td>{4}</td>
			<td>{5}</td>
			
			</tr>""".format(str(item['row']),str(item['item_code']),str(item['warehouse']),str(item['warehouse_qty']),str(item['qty']),str(float(item['warehouse_qty'])-float(item['qty'])),str(item['Item']))
		table = table + """</table>"""
		frappe.msgprint(table)
	else:
		frappe.msgprint("Stock is available for all Item!")
