from __future__ import unicode_literals
import json
import frappe, erpnext
from frappe import _, scrub
from frappe.utils import cint, flt, round_based_on_smallest_currency_fraction
from erpnext.controllers.accounts_controller import validate_conversion_rate, \
	validate_taxes_and_charges, validate_inclusive_tax
from erpnext.stock.utils import get_stock_balance


def validate(self,method):
	item_stock_issue(self)
	check_stock(self)

def check_stock(self):
	over_stock = 0
	out_of_stock = []
	for item in self.items:
		
		allow_negative = frappe.db.get_value("Warehouse",item.warehouse,"allow_negative_stock")
		if not allow_negative:
			warehouse_stock = get_stock_balance(item.item_code,item.warehouse)
			maintain_stock = frappe.db.get_value("Item",item.item_code,"is_stock_item")
		
			if item.qty > warehouse_stock and maintain_stock:
				over_stock = 1
				out_of_stock.append({
					"item_code":item.item_code,
					"warehouse":item.warehouse,
					"qty":item.qty,
					"warehouse_qty":warehouse_stock,
					"row":item.idx
				})
	if over_stock == 1:
		
		table = """<h3>Negative Stock Alert</h3><br><table class="table-bordered"><tr>
		<th>Row No.</th>
		<th>Item Code</th>
		<th>Warehouse</th>
		<th>Warehouse Qty</th>
		<th>Ordered Qty</th>
		<th>Short Qty</th>
		</tr>"""
		for item in out_of_stock:
			table = table + """<tr>
			<td>{0}</td>
			<td>{1}</td>
			<td>{2}</td>
			<td>{3}</td>
			<td>{4}</td>
			<td>{5}</td>
			</tr>""".format(str(item['row']),str(item['item_code']),str(item['warehouse']),str(item['warehouse_qty']),str(item['qty']),str(float(item['warehouse_qty'])-float(item['qty'])))
		table = table + """</table>"""
		frappe.throw(table)

        
def item_stock_issue(self):
	stock_issue = []
	for item in self.items:
		if frappe.db.get_value("Item",item.item_code,"stock_issue"):
			stock_issue.append(item.item_code)
	
	if len(stock_issue) > 0:
		for item in stock_issue:
			frappe.msgprint("Item {} has stock Issue!".format(item))
		frappe.throw("Please Contact Administrator!")