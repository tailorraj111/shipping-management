import frappe

def on_submit(self,method):
	
	for item in self.items:
		state = frappe.db.get_value("Item",item.item_code,"workflow_state")
		if state != "Approved":
			frappe.throw("Your Purchase Receipt can not be Approved as {} is not approved Item.".format(item.item_code))