from __future__ import unicode_literals
import json
import frappe
from frappe import _
from frappe.utils import  flt
from shipping_management.override.taxes_and_totals import calculate_taxes_and_totals

def validate(self,method):
	if self.gst_category not in ["Overseas", "Deemed Export"]:  
		if len(self.taxes) == 0 or self.total_taxes_and_charges == 0.00:  
			frappe.throw("Please Add Taxes!!")  

	# -------------------------------------------------Shipping Customization--------------------------------------------------------------------
	update_item_total(self)
	calculate_taxes_and_totals(self)
	# -------------------------------------------------Shipping Customization-------------------------------------------------------------------


def update_item_total(self):
	if self.shipping_method:
		shipping = 0
		for item in self.items:
			if item.item_group == "Shipping Item":
				shipping = shipping + item.amount
		
		self.item_total = self.net_total - shipping
	else:
		self.item_total = self.net_total

@frappe.whitelist()
def apply_shipping_rule_custom(curr_doc):
	doc = json.loads(curr_doc)
	shipping_rule = frappe.get_doc("Shipping Rule", doc["shipping_rule_custom"])
	
	shipping_amount = 0.0
	by_value = False

	shipping_rule.validate_countries(doc)

	if shipping_rule.calculate_based_on == 'Net Total':
		value = doc["base_net_total"]
		by_value = True

	elif shipping_rule.calculate_based_on == 'Net Weight':
		value = doc["total_net_weight"]
		by_value = True

	elif shipping_rule.calculate_based_on == 'Fixed':
		shipping_amount = shipping_rule.shipping_amount

	# shipping amount by value, apply conditions
	if by_value:
		shipping_amount = shipping_rule.get_shipping_amount_from_rules(value)

	# convert to order currency
	if doc["currency"] != frappe.db.get_value("Global Defaults",None,"default_currency"):
		shipping_amount = flt(shipping_amount / doc["conversion_rate"], 2)
	
	return shipping_amount

        
