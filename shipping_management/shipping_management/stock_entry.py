import frappe
from erpnext.stock.utils import get_stock_balance
from frappe.utils import cint
import json
import traceback
from erpnext.stock.doctype.serial_and_batch_bundle.serial_and_batch_bundle import create_serial_nos

def validate(self,method):
    if self.stock_entry_type == "Manufacture":
        value = 0
        for item in self.items:
            if item.is_finished_item == 0:
                value = value + (item.basic_rate * (item.qty / self.fg_completed_qty))
            else:
                item.basic_rate = value
                item.valuation_rate = value
                item.basic_amount = value * item.qty
                item.amount = value * item.qty
                self.total_incoming_value = value * item.qty
                self.value_difference = (value * item.qty) - self.total_outgoing_value
                
    check_stock(self)
    check_warehouse_permission(self)


def check_stock(self):
    over_stock = 0
    out_of_stock = []
    for item in self.items:
        if item.s_warehouse:
            allow_negative = frappe.db.get_value("Warehouse",item.s_warehouse,"allow_negative_stock")
            if not allow_negative:
                warehouse_stock = get_stock_balance(item.item_code,item.s_warehouse)
                maintain_stock = frappe.db.get_value("Item",item.item_code,"is_stock_item")
                # frappe.msgprint(str(warehouse_stock)+" "+str(item.qty))
                if item.qty > warehouse_stock and maintain_stock:
                    over_stock = 1
                    out_of_stock.append({
                        "item_code":item.item_code,
                        "warehouse":item.s_warehouse,
                        "qty":item.qty,
                        "warehouse_qty":warehouse_stock,
                        "row":item.idx
                    })
    if over_stock == 1:
        # frappe.throw(str(out_of_stock))
        table = """<h3>Negative Stock Alert</h3><br><table class="table-bordered"><tr>
        <th>Row No.</th>
        <th>Item Code</th>
        <th>Warehouse</th>
        <th>Warehouse Qty</th>
        <th>Ordered Qty</th>
        <th>Short Qty</th>
        </tr>"""
        for item in out_of_stock:
            table = table + """<tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            <td>{3}</td>
            <td>{4}</td>
            <td>{5}</td>
            </tr>""".format(str(item['row']),str(item['item_code']),str(item['warehouse']),str(item['warehouse_qty']),str(item['qty']),str(float(item['warehouse_qty'])-float(item['qty'])))
        table = table + """</table>"""
        
        if self.stock_entry_type == "Material Transfer for Manufacture" and self.workflow_state in ["Draft","Material Requested"]:
            frappe.msgprint(table)
        else:
            frappe.throw(table)

def check_warehouse_permission(self):
    # user = "chetan@trueview.co.in"
    user = frappe.session.user
    if frappe.session.user == "Administrator":
        return

    from_warhouses = frappe.db.sql("""
    select wpt.from_warehouse
    from `tabWarehouse Permission From WH` wpt 
    where wpt.user= %s
    """,(user), as_dict = 1 )

    from_warehouse_list = []

    for item in from_warhouses:
        from_warehouse_list.append(item.from_warehouse)

    if self.from_warehouse:
        if self.from_warehouse not in from_warehouse_list:
            frappe.throw("Default Source Warehouse " + self.from_warehouse + " is not assigned to you.")

    to_warhouses = frappe.db.sql("""
    select wpt.to_warehouse
    from `tabWarehouse Permission To WH` wpt 
    where wpt.user= %s
    """,(user), as_dict = 1 )

    to_warehouse_list = []

    for item in to_warhouses:
        to_warehouse_list.append(item.to_warehouse)

    if self.to_warehouse:
        if self.to_warehouse not in to_warehouse_list:
            frappe.throw("Default Target Warehouse " + self.to_warehouse + " is not assigned to you.")

@frappe.whitelist()
def send_email_notification(stock_entry,out_going_entry):
    pretransfered = frappe.db.get_value("Stock Entry",out_going_entry,"per_transferred")
    if pretransfered < 100:
        stock_voucher = frappe.get_doc("Stock Entry",out_going_entry)
        stock_items = []
        for item in stock_voucher.items:
            if item.qty != item.transferred_qty:
                stock_items.append({
                    "item_code":item.item_code,
                    "item_name":item.item_name,
                    "tarnsfered":item.qty,
                    "received":item.transferred_qty
                })
                # frappe.msgprint(str(item.transferred_qty))
        # frappe.throw(str(stock_items))
        args = {
            "outward":out_going_entry,
            "inward":stock_entry,
            "stock_items":stock_items
        }
        user_list = frappe.get_list("User",[["Has Role","role","=","Stock Manager"],["enabled","=",1]],["name"])
        # frappe.msgprint(str(user_list))
        for user in user_list:
            frappe.msgprint(user["name"])
            frappe.sendmail(
                recipients=user["name"],
                subject="Stock Mismatch Alert",
                template="stock_transfer_alert",
                args=args)

@frappe.whitelist()
def get_serial(voucher_no):
    voucher = frappe.get_doc("Serial Number Voucher", voucher_no)
    serials = []
    raw_serial = []
    batch = voucher.items[0].batch_number
    raw_material_item = voucher.items[0].raw_material_item
    for item in voucher.items:
        if frappe.db.exists("Serial No", {"name": item.serial_number, "status": "Inactive"}):
            serials.append(item.serial_number)
            raw_serial.append(item.raw_material_sn)
    
    return serials, voucher.item_code,batch,raw_serial,raw_material_item


@frappe.whitelist()
def create_serial_batch_bundle(serial_number_voucher,item_code,qty,rate,warehouse,total_amount,child_name,parent,parenttype):
    try:
        serial_no_voucher = frappe.get_doc("Serial Number Voucher", serial_number_voucher)
        if not serial_no_voucher:
            frappe.throw("Can't Find Serial Number Voucher")
        
        sbb_doc = frappe.new_doc("Serial and Batch Bundle")
        sbb_doc.item_code = item_code
        sbb_doc.has_serial_no = 1
        sbb_doc.has_batch_no = 1
        sbb_doc.warehouse = warehouse
        sbb_doc.type_of_transaction = "Inward"
        sbb_doc.total_qty = float(qty)
        sbb_doc.voucher_type = parenttype
        sbb_doc.voucher_no = parent
        sbb_doc.avg_rate = float(rate)
        sbb_doc.total_amount = float(total_amount)
        sbb_doc.voucher_detail_no = child_name

        serial_numbers = frappe.db.sql("""
        select snvi.serial_number
        from`tabSerial Number Voucher Item` snvi
        Inner Join`tabSerial No` sn on sn.name = snvi.serial_number
        where sn.status = 'Inactive' and snvi.parent = %(parent_voucher)s order by snvi.idx asc
        """,{"parent_voucher":serial_number_voucher},as_dict=1)
        
        if not serial_numbers:
            frappe.throw("Can't Find Serial No")

        entries = []
        found_serials = 0
        for item in serial_numbers:
            if found_serials >= int(qty):
                break
            serial_number = item.get("serial_number")
            if serial_number:
                entry = {
                    "serial_no": serial_number,
                    "qty": 1,
                    "warehouse": warehouse,
                    "incoming_rate": float(rate),
                    "stock_value_difference": float(rate)
                }
                entries.append(entry)
                found_serials += 1
        
        sbb_doc.set("entries", entries)
        sbb = sbb_doc.save(ignore_permissions=1)
        
        frappe.db.set_value("Stock Entry Detail", {'parent': parent, 'is_finished_item': "1"}, 'serial_and_batch_bundle', sbb.name)
        frappe.msgprint("Serial and Batch Bundle created successfully.")

    except Exception as e:
        traceback_msg = traceback.format_exc()
        frappe.log_error(title="create_serial_batch_bundle_error", message=traceback_msg)
        frappe.throw("An error occurred while creating Serial and Batch Bundle. Check error logs for details.")


@frappe.whitelist()
def duplicate_serial_batch_bundle(item_code,sn_bundle):
    sn_doc = frappe.get_doc("Serial and Batch Bundle", sn_bundle)
    sn_list = []
    for item in sn_doc.entries:
        sn_list.append("T-"+item.serial_no)
    sn_lists = create_serial_nos(item_code, sn_list)
    # frappe.msgprint(str(sn_lists))
    return sn_lists
