import frappe
from frappe import _

def validate(self,method):
    # frappe.throw("here")
    for item in self.locations:
        serial_nos = get_serial_nos(item.serial_no)
        # frappe.msgprint(str(serial_nos))
        # if len(serial_nos)>0:
        frappe.msgprint(str(serial_nos))
        if len(serial_nos) and len(serial_nos) != abs(int(item.picked_qty)):
            frappe.throw(_("{0} Serial Numbers required for Item {1}. You have provided {2}.").format(abs(item.picked_qty),item.item_code, len(serial_nos)))
        if len(serial_nos) != len(set(serial_nos)):
            frappe.throw(_("Duplicate Serial No entered for Item {0}").format(item.item_code))

        for serial_no in serial_nos:
            # frappe.msgprint(str(serial_no))
            if frappe.db.exists("Serial No", serial_no):
                # frappe.msgprint("serial number exist")
                sr = frappe.db.get_value("Serial No", serial_no, ["name", "item_code", "batch_no", "sales_order",
                    "delivery_document_no", "delivery_document_type", "warehouse", "purchase_document_type",
                    "purchase_document_no", "company", "status"], as_dict=1)
                # frappe.msgprint(str(item.item_code))
                # frappe.msgprint(str(sr.item_code))
                if sr.item_code!=item.item_code:
                    frappe.throw(_("Serial No {0} does not belong to Item {1}").format(serial_no,item.item_code))
                # frappe.msgprint(str(item.warehouse))
                # frappe.msgprint(str(sr.warehouse))
                if sr.warehouse!=item.warehouse:
                    frappe.throw(_("Serial No {0} does not belong to Warehouse {1}").format(serial_no,
                    item.warehouse))
            else:
                frappe.throw("Serial Number {0} not Exist".format(serial_no))
    # frappe.throw("here")






def get_serial_nos(serial_no):
	if isinstance(serial_no, list):
		return serial_no

	return [s.strip() for s in str(serial_no).strip().upper().replace(',', '\n').split('\n')
		if s.strip()]