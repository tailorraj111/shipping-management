# Copyright (c) 2024, Raj Tailor and contributors
# For license information, please see license.txt

import frappe


def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def get_data(filters):

	sales_order_data = frappe.db.sql("""
		SELECT so.customer as customer, so.name as sales_order,so.net_total as sales_order_amount, soi.item_code as sales_item, soi.amount as sales_item_total, so.transaction_date as sales_order_date, dn.name as delivery_note,dn.posting_date as delivery_date,dni.qty as delivery_qty, dni.amount as delivery_amount, si.posting_date as invoice_date, si.name as invoice, sii.qty as invoice_qty, sii.amount as invoice_amount from
		`tabSales Order` so inner join `tabSales Order Item` soi on so.name = soi.parent 
		left join `tabDelivery Note Item` dni on dni.so_detail = soi.name 
		left join `tabDelivery Note` dn on dni.parent = dn.name 
		left join `tabSales Invoice Item` sii on soi.name = sii.so_detail 
		left join `tabSales Invoice` si on sii.parent = si.name
		where so.docstatus = 1 {conditions}
	""".format(conditions=get_conditions(filters)), filters, as_dict=1)
	return sales_order_data

def get_conditions(filters):
	conditions = ""
	if filters.get("from_date"):
		conditions += " and so.transaction_date >= %(from_date)s"
	if filters.get("to_date"):
		conditions += " and so.transaction_date <= %(to_date)s"
	if filters.get("customer"):
		conditions += " and so.customer = %(customer)s"
	if filters.get("sales_order"):
		conditions += " and so.name = %(sales_order)s"
	return conditions

def get_columns(filters):
	columns = [
		{
			"fieldname": "customer",
			"label": "Customer",
			"fieldtype": "Link",
			"options": "Customer",
			"width": 150
		},
		{
			"fieldname": "sales_order",
			"label": "Sales Order",
			"fieldtype": "Link",
			"options": "Sales Order",
			"width": 150
		},
		{
			"fieldname": "sales_order_date",
			"label": "Sales Order Date",
			"fieldtype": "Date",
			"width": 150
		},
		{
			"fieldname": "sales_order_amount",
			"label": "Sales Order Amount",
			"fieldtype": "Currency",
			"width": 150
		},
		{
			"sales_item": "sales_item",
			"label": "Sales Item",
			"fieldtype": "Link",
			"options": "Item",
			"width": 150
		},
		{
			"fieldname": "sales_item_total",
			"label": "Sales Item Total",
			"fieldtype": "Currency",
			"width": 150
		},
		{
			"fieldname": "delivery_date",
			"label": "Delivery Date",
			"fieldtype": "Date",
			"width": 150
		},
		{
			"fieldname": "delivery_note",
			"label": "Delivery Note",
			"fieldtype": "Link",	
			"options": "Delivery Note",
			"width": 150
		},
		{
			"fieldname": "delivery_qty",
			"label": "Delivery Qty",
			"fieldtype": "Float",
			"width": 150
		},
		{
			"fieldname": "delivery_amount",
			"label": "Delivery Amount",
			"fieldtype": "Currency",
			"width": 150
		},
		{
			"fieldname": "invoice_date",
			"label": "Invoice Date",	
			"fieldtype": "Date",
			"width": 150
		},
		{
			"fieldname": "invoice",
			"label": "Invoice",
			"fieldtype": "Link",
			"options": "Sales Invoice",
			"width": 150
		},
		{
			"fieldname": "invoice_qty",
			"label": "Invoice Qty",
			"fieldtype": "Float",
			"width": 150
		},
		{
			"fieldname": "invoice_amount",
			"label": "Invoice Amount",
			"fieldtype": "Currency",
			"width": 150
		}]

	return columns		
		


