# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_column(filters), get_data(filters)
	return columns, data


def get_data(filters):
	data = []
	get_built_data = frappe.db.sql("""
	select bi.parent,b.item as item_code,b.item_name,bn.warehouse,bn.actual_qty as avl_qty,"Built" as type from `tabBOM Item` bi inner join `tabBOM` b on bi.parent = b.name inner join `tabBin` as bn on bn.item_code = b.item where bi.item_code = %s and b.is_active = 1 and bn.actual_qty > 0
	""",(filters.get("item")),as_dict=1)

	get_parent_data = frappe.db.sql("""
	select bn.item_code,it.item_name,bn.warehouse,bn.actual_qty as avl_qty,"Main" as type from `tabBin` as bn inner join `tabItem` as it on it.name = bn.item_code where bn.item_code = %s and bn.actual_qty > 0
	""",(filters.get("item")),as_dict=1)
	
	data = get_parent_data + get_built_data


	return data

	frappe.msgprint(str(get_data))

def get_column(filters):

	return[
		
		{
			"label": _("Item Code"),
			"fieldname": "item_code",
			"fieldtype": "Link",
			"options":"Item",
			"width": 150
		},
		{
			"label": _("Item Name"),
			"fieldname": "item_name",
			"fieldtype": "Data",
			"width": 250
		},
		{
			"label": _("Warehouse"),
			"fieldname": "warehouse",
			"fieldtype": "Data",
			"width": 100
		},
		{
			"label": _("Type"),
			"fieldname": "type",
			"fieldtype": "Data",
			"width": 100
		},
		
		{
			"label": _("Available Qty"),
			"fieldname": "avl_qty",
			"fieldtype": "float",
			"width": 100
		},
		
	]
