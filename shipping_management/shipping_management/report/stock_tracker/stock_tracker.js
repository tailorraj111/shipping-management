// Copyright (c) 2016, Raj Tailor and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Stock Tracker"] = {
	"filters": [
		{
			"fieldname":"item",
			"label": __("Item Code"),
			"fieldtype": "Link",
			"options": "Item",
			"reqd":1
		},
		{
			"fieldname":"item_group",
			"label": __("Item Group"),
			"fieldtype": "Link",
			"options": "Item Group"
		},
		
	]
};
