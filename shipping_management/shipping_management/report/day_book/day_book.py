# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from multiprocessing import Condition
import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def get_data(filters):
	data = []
	if not filters.get("voucher_type") or filters.get("voucher_type") == "Sales Invoice":
		sales_invoice_data= frappe.db.sql("""
			select "Sales Invoice" as voucher_type,name as voucher_name ,Date(posting_date) as Date ,owner, rounded_total as amount from `tabSales Invoice` where docstatus=1 {conditions} 
			order by posting_date desc
		""".format(conditions=get_conditions(filters)),filters,as_dict=1)
	else:
		sales_invoice_data = []
	# frappe.throw(str(sales_invoice_data))
	# frappe.msgprint(str(sales_invoice_data))

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Sales Order":
		conditions = []

		if filters.get("Date(creation)"):
			conditions.append("Date(transaction_date)=%(Date(creation))s")

		if filters.get("owner"):
			conditions.append("owner=%(owner)s")

		condition = "and {}".format(" and ".join(conditions)) if conditions else ""

		sales_order_data= frappe.db.sql("""
			select "Sales Order" as voucher_type,name as voucher_name ,Date(transaction_date) as Date ,owner, rounded_total as amount from `tabSales Order` where docstatus = 1 {conditions}
			order by transaction_date desc
		""".format(conditions=condition),filters,as_dict=1)
	else:
		sales_order_data = []

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Delivery Note":
		delivery_note_data= frappe.db.sql("""
			select "Delivery Note" as voucher_type,name as voucher_name ,Date(posting_date) as Date ,owner, rounded_total as amount from `tabDelivery Note` where docstatus = 1 {conditions}
			order by posting_date desc
		""".format(conditions=get_conditions(filters)),filters,as_dict=1)
	else:
		delivery_note_data =[]
    
	if not filters.get("voucher_type") or filters.get("voucher_type") == "Purchase Order":
		conditions = []

		if filters.get("Date(creation)"):
			conditions.append("Date(transaction_date)=%(Date(creation))s")

		if filters.get("owner"):
			conditions.append("owner=%(owner)s")

		condition = "and {}".format(" and ".join(conditions)) if conditions else ""
		purchase_order_data= frappe.db.sql("""
			select "Purchse Order" as voucher_type,name as voucher_name ,Date(transaction_date) as Date ,owner, rounded_total as amount from `tabPurchase Order` where docstatus = 1 {conditions}
			order by transaction_date desc
		""".format(conditions=condition),filters,as_dict=1)
	else:
		purchase_order_data = []

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Purchase Receipt":
		purchase_receipt_data= frappe.db.sql("""
			select "Purchase Receipt" as voucher_type,name as voucher_name ,Date(posting_date) as Date ,owner, rounded_total as amount from `tabPurchase Receipt` where docstatus = 1 {conditions}
			order by posting_date desc
		""".format(conditions=get_conditions(filters)),filters,as_dict=1)
	else:
		purchase_receipt_data = []

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Purchase Invoice":
		purchase_invoice_data= frappe.db.sql("""
		select "Purchase Invoice" as voucher_type,name as voucher_name ,Date(posting_date) as Date ,owner, rounded_total as amount from `tabPurchase Invoice` where docstatus = 1 {conditions}
		order by posting_date desc
		""".format(conditions=get_conditions(filters)),filters,as_dict=1)
	else:
		purchase_invoice_data =[]	

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Work Order":
		conditions = []

		if filters.get("Date(creation)"):
			conditions.append("Date(creation)=%(Date(creation))s")

		if filters.get("owner"):
			conditions.append("owner=%(owner)s")

		condition = "and {}".format(" and ".join(conditions)) if conditions else ""
		work_order_data= frappe.db.sql("""
		select "Work Order" as voucher_type,name as voucher_name ,Date(creation) as Date ,owner from `tabWork Order` where docstatus = 1 {conditions}
		order by planned_start_date desc
		""".format(conditions=condition),filters,as_dict=1)
	else:
		work_order_data =[]

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Stock Entry":
		stock_entry_data= frappe.db.sql("""
		select "Stock Entry" as voucher_type,name as voucher_name ,Date(posting_date) as Date ,owner, value_difference as amount from `tabStock Entry` where docstatus = 1 {conditions}
		order by posting_date desc
		""".format(conditions=get_conditions(filters)),filters,as_dict=1)
	else:
		stock_entry_data = []

	if not filters.get("voucher_type") or filters.get("voucher_type") == "Journal Entry":
		journal_entry_data= frappe.db.sql("""
		select "Journal Entry" as voucher_type,name as voucher_name ,Date(posting_date) as Date ,owner, total_credit as amount from `tabJournal Entry` where docstatus = 1 {conditions}
		order by posting_date desc
		""".format(conditions=get_conditions(filters)),filters,as_dict=1)
	else:
		journal_entry_data = []
	# # frappe.msgprint(str(data))
	sales_invoice = sales_invoice_data+sales_order_data+delivery_note_data+purchase_order_data+purchase_receipt_data+purchase_invoice_data+work_order_data+stock_entry_data+journal_entry_data
	return sales_invoice

def get_conditions(filters):
	conditions = []

	if filters.get("Date(creation)"):
		conditions.append("Date(posting_date)=%(Date(creation))s")

	# if filters.get("voucher_type"):
	# 	conditions.append("voucher_type=%(voucher_type)s")

	if filters.get("owner"):
		conditions.append("owner=%(owner)s")

	return "and {}".format(" and ".join(conditions)) if conditions else ""


def get_columns(filters):
	

	columns = [
		{
			"label":_("Voucher Type"),
			"fieldname": "voucher_type",
			"fieldtype": "Data",
			"width": 150
		},
		{
			"label":_("Date"),
			"fieldname": "Date",
			"fieldtype": "Date",
			"width": 120
		},
		{
			"label":_("User"),
			"fieldname": "owner",
			"fieldtype": "Data",
			"width": 230
		},
	]
	if filters.get("voucher_type"):
		columns.append({
			"label":_("Voucher Name"),
			"fieldname": "voucher_name",
			"fieldtype": "Link",
			"options":filters.get("voucher_type"),
			"width": 150
		})
	else:
		columns.append({
			"label":_("Voucher Name"),
			"fieldname": "voucher_name",
			"fieldtype": "Data",
			"width": 150
		})

	columns.append({
		"label":_("Amount"),
		"fieldname": "amount",
		"fieldtype": "Data",
		"width": 150
	})
	return columns
