// Copyright (c) 2016, Raj Tailor and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Day Book"] = {
	"filters": [
		{
			fieldname:"voucher_type",
			label:__("Voucher Type"),
			fieldtype:"Link",
			options:"DocType"
		},
		{
			fieldname:"Date(creation)",
			label:__("Date"),
			fieldtype:"Date"
		},
		{
			fieldname:"owner",
			label:__("User"),
			fieldtype:"Link",
			options:"User"
		}
	]
};
