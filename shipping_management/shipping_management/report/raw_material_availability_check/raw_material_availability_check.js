// Copyright (c) 2016, Raj Tailor and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Raw Material Availability Check"] = {
	"filters": [
		{
			"fieldname":"item",
			"label": __("Item To Be Manufacture"),
			"fieldtype": "Link",
			"options":"Item",
			"reqd":1,
			"width": "80"
		},
		{
			"fieldname":"warehouse",
			"label": __("Warehouse"),
			"fieldtype": "Link",
			"options":"Warehouse",
			"reqd":1,
			"width": "80"
		},
		{
			"fieldname":"qty",
			"label": __("Qty"),
			"fieldtype": "Int",
			"reqd":1,
			"width": "80"
		},

	]
};
