# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_columns(filters), get_data(filters)
	return columns, data

def get_data(filters):
	data = frappe.db.sql("""
	select
	o.name as sales_order,
	o.customer as cust_name,
	c.payment_terms as payment_terms,
	o.transaction_date as so_date,
	o.rounded_total as amount,
	o.sales_person as sales_person,
	o.commitment_date as commitment_date,
	o.actual_payment_date as actual_date,
	if(o.actual_payment_date is null, DATEDIFF(now(),o.commitment_date), DATEDIFF(o.actual_payment_date , o.commitment_date)) as delay_no
	from `tabSales Order` o
	inner join `tabCustomer` c on c.customer_name = o.customer
	where o.commitment_date is not null
	or
	o.actual_payment_date is not null
	order by o.commitment_date desc
	""",as_dict = 1)
	return data

def get_columns(filters):
	columns = [
		{
			"label":_("Sales Order"),
			"fieldname": "sales_order",
			"fieldtype": "Link",
			"options": "Sales Order",
			"width": 100
		},
		{
			"label":_("Cust Name"),
			"fieldname": "cust_name",
			"fieldtype": "Data",
			"width": 250
		},
		{
			"label":_("Terms"),
			"fieldname": "payment_terms",
			"fieldtype": "Data",
			"width": 200
		},
		{
			"label":_("SO Date"),
			"fieldname": "so_date",
			"fieldtype": "Date",
			"width": 100
		},
		{
			"label":_("Amount"),
			"fieldname": "amount",
			"fieldtype": "Float",
			"width": 100
		},
		{
			"label":_("Sales Person"),
			"fieldname": "sales_person",
			"fieldtype": "Data",
			"width": 150
		},
		{
			"label":_("Commitment Date"),
			"fieldname": "commitment_date",
			"fieldtype": "Date",
			"width": 100
		},
		{
			"label":_("Actual Date"),
			"fieldname": "actual_date",
			"fieldtype": "Date",
			"width": 100
		},
		{
			"label":_("Delay No. of Days"),
			"fieldname": "delay_no",
			"fieldtype": "Int",
			"width": 100
		},
	]

	return columns
