# Copyright (c) 2024, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_colums(filters), get_data(filters)
	return columns, data


def get_data(filters):
	conditions = get_conditions(filters)
	
	query = f"""
		SELECT
			sbb.name,
			sbe.serial_no,
			sbb.item_code,
			sbb.item_name,
			sbb.posting_date,
			sbb.voucher_type,
			sbb.voucher_no,
			CASE
				WHEN sbb.voucher_type = 'Sales Invoice' THEN si.customer
				WHEN sbb.voucher_type = 'Delivery Note' THEN dn.customer
			END AS customer_name,
			CASE
				WHEN sbb.voucher_type = 'Sales Invoice' THEN si.territory
				WHEN sbb.voucher_type = 'Delivery Note' THEN dn.territory
			END AS territory
		FROM
			`tabSerial and Batch Bundle` sbb
		INNER JOIN
			`tabSerial and Batch Entry` sbe ON sbb.name = sbe.parent
		LEFT JOIN
			`tabSales Invoice` si ON sbb.voucher_type = 'Sales Invoice' AND sbb.voucher_no = si.name
		LEFT JOIN
			`tabDelivery Note` dn ON sbb.voucher_type = 'Delivery Note' AND sbb.voucher_no = dn.name
		WHERE
			sbb.docstatus = 1
			AND sbb.type_of_transaction = "Outward"
			AND sbb.voucher_type IN ("Sales Invoice", "Delivery Note")
			AND sbb.posting_date BETWEEN %(from_date)s AND %(to_date)s
			AND {conditions}
	"""
	
	serial_batch_bundle_details = frappe.db.sql(query, filters, as_dict=True)
	return serial_batch_bundle_details

def get_conditions(filters):
	conditions = []
	
	if filters.get("from_date"):
		conditions.append("sbb.posting_date >= %(from_date)s")
	
	if filters.get("to_date"):
		conditions.append("sbb.posting_date <= %(to_date)s")
	
	if filters.get("customer"):
		conditions.append("""
			(
				(sbb.voucher_type = 'Sales Invoice' AND si.customer = %(customer)s)
				OR
				(sbb.voucher_type = 'Delivery Note' AND dn.customer = %(customer)s)
			)
		""")
	
	if filters.get("serial_no"):
		conditions.append("sbe.serial_no = %(serial_no)s")

	return " AND ".join(conditions) if conditions else "1=1"



def get_colums(filters):
	columns = [
		
		{
			"label":_("Serial Batch Bundle"),
			"fieldname": "name",
			"fieldtype": "Link",
			"options":"Serial and Batch Bundle",
			"width": 150
		},
		{
			"label":_("Customer"),
			"fieldname": "customer_name",
			"fieldtype": "Link",
			"options":"Customer",
			"width": 200
		},
		{
			"label":_("Territory"),
			"fieldname": "territory",
			"fieldtype": "Link",
			"options":"Territory",
			"width": 150
		},
		{
			"label":_("Invoice/DC Number"),
			"fieldname": "voucher_no",
			"fieldtype": "Data",
			"width": 160
		},
		{
			"label":_("Invoice Date"),
			"fieldname": "posting_date",
			"fieldtype": "Date",
			"width": 120
		},
		
		{
			"label":_("Item Code"),
			"fieldname": "item_code",
			"fieldtype": "Link",
			"options":"Item",
			"width": 150
		},
		{
			"label":_("Item Name"),
			"fieldname": "item_name",
			"fieldtype": "Data",
			"width": 160
		},
		{
			"label":_("Serial Number"),
			"fieldname": "serial_no",
			"fieldtype": "Data",
			"width": 120
		},
		
	]
	return columns