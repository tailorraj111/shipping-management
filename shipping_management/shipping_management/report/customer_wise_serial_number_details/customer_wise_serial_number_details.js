// Copyright (c) 2024, Raj Tailor and contributors
// For license information, please see license.txt

frappe.query_reports["Customer Wise Serial Number Details"] = {
	"filters": [
		{
			fieldname:"from_date",
			label: __("From Date"),
			fieldtype: "Date",
			default: frappe.datetime.month_start(),
			reqd: 1,
			on_change:function(){
				var from_date = frappe.query_report.get_filter_value('from_date');
				var to_date = frappe.query_report.get_filter_value('to_date'); 
				
				if (from_date > to_date){
					frappe.throw("From Date must be before To Date")
				}
				var date_difference = frappe.datetime.get_diff(to_date, from_date);
				if (date_difference > 31) {
					frappe.throw("Date range exceeds maximum of 31 days.");
					return;
				}
			}
		},
		{
			fieldname:"to_date",
			label: __("To Date"),
			fieldtype: "Date",
			default: frappe.datetime.month_end(),
			reqd: 1,
			on_change:function(){
				var from_date = frappe.query_report.get_filter_value('from_date');
				var to_date = frappe.query_report.get_filter_value('to_date'); 
				if (from_date > to_date){
					frappe.msgprint("From Date must be before To Date")
					return;
				}
				var date_difference = frappe.datetime.get_diff(to_date, from_date);
				if (date_difference > 31) {
					frappe.msgprint("Date range exceeds maximum of 31 days.");
					return;
				}
			}
		},
		{
			fieldname:"serial_no",
			label: __("Serial No"),
			fieldtype: "Link",
			options:"Serial No"
		},
		{
			fieldname:"customer",
			label: __("Customer"),
			fieldtype: "Link",
			options:"Customer"
		},
	]
};
