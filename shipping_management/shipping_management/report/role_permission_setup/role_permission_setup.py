# Copyright (c) 2024, Raj Tailor and contributors
# For license information, please see license.txt

import frappe

def execute(filters=None):
	columns, data = get_coloum(filters), get_data(filters)
	return columns, data


def get_data(filters):
	
	conditions = get_conditions(filters)
	role_profile = filters.get("role_profile")

	role_list_from_profile = frappe.db.sql("""
	SELECT role FROM `tabHas Role` WHERE parenttype = "Role Profile" AND parent = %(role_profile)s
	""", {"role_profile": role_profile}, as_dict=True)

	roles = [r.role for r in role_list_from_profile]
	
	docperm = frappe.db.sql("""
	SELECT cd.role, cd.parent, cd.permlevel, cd.share, cd.export, cd.cancel, cd.create, cd.submit, cd.write, cd.print, cd.import, cd.email, cd.read, cd.report, cd.amend, cd.delete, cd.select
	FROM `tabCustom DocPerm` cd
	WHERE cd.role IN %(roles)s {}
	""".format(conditions), {"roles": roles, **filters}, as_dict=True)

	return docperm




def get_conditions(filters):
	conditions = ""

	if filters.get("role"):
		conditions += " and cd.role = %(role)s"
	if filters.get("doctype"):
		conditions += " and cd.parent = %(doctype)s"
	
	return conditions



def get_coloum(filters):
	columns = [
		{
			"fieldname": "role",
			"label": "Role",
			"fieldtype": "Link",
			"options": "Role",
			"width": 150
		},
		{
			"fieldname": "parent",
			"label": "Doctype",
			"fieldtype": "Link",
			"options": "DocType",
			"width": 150
		},
		{
			"fieldname": "permlevel",
			"label": "Perm Level",
			"fieldtype": "Int",
			"width": 100
		},
		{
			"fieldname": "share",
			"label": "Share",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "export",
			"label": "Export",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "cancel",
			"label": "Cancel",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "create",
			"label": "Create",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "submit",
			"label": "Submit",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "write",
			"label": "Write",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "print",
			"label": "Print",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "import",
			"label": "import",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "email",
			"label": "Email",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "read",
			"label": "Read",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "report",
			"label": "Report",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "amend",
			"label": "Amend",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "delete",
			"label": "Delete",
			"fieldtype": "Check",
			"width": 100
		},
		{
			"fieldname": "select",
			"label": "Select",
			"fieldtype": "Check",
			"width": 100
		},
	]

	return columns