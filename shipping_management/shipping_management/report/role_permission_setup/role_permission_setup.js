// Copyright (c) 2024, Raj Tailor and contributors
// For license information, please see license.txt

frappe.query_reports["Role Permission Setup"] = {
	"filters": [
		{
			fieldname: "role_profile",
			label: __("Role Profile"),
			fieldtype: "Link",
			options: "Role Profile",
			reqd: 1,
			onchange: function() {
                var show_role_filters = !!(this.value);
                frappe.query_report.toggle_display(["role", "doctype"], show_role_filters);
            }
		},
		{
			fieldname: "role",
			label: __("Role"),
			fieldtype: "Link",
			options: "Role",
			depends_on: "eval: doc.role_profile"
		},
		{
			fieldname: "doctype",
			label: __("Doctype"),
			fieldtype: "Link",
			options: "DocType",
			depends_on: "eval: doc.role_profile"
		},
	]
};
