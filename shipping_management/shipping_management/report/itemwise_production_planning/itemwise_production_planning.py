# Copyright (c) 2024, Raj Tailor and contributors
# For license information, please see license.txt

import frappe
from frappe import _

def execute(filters=None):
	columns, data = get_coloums(filters), get_data(filters)
	return columns, data

def get_data(filters):
    from_date = filters.get("from_date")
    to_date = filters.get("to_date")
    warehouse = filters.get("warehouse")

    if from_date > to_date:
        frappe.throw(_("From Date cannot be greater than To Date"))

    data = frappe.db.sql("""
        SELECT
            woi.item_code,
            woi.item_name,
            SUM(woi.required_qty) AS total_qty,
            GROUP_CONCAT(DISTINCT wo.name) AS wo_name,
			wo.planned_start_date
        FROM
            `tabWork Order` wo
            LEFT JOIN `tabWork Order Item` woi ON wo.name = woi.parent
        WHERE
            wo.docstatus < 2
            AND wo.planned_start_date BETWEEN %s AND %s
        GROUP BY
            woi.item_code
    """, (from_date, to_date),as_dict=1)

    processed_data = []

    for row in data:
        
        available_qty = get_available_qty(row.get("item_code"), warehouse)
        expected_qty = get_expected_qty(row.get("item_code"), from_date, to_date)
        short_qty = row.get("total_qty") - available_qty - expected_qty
        processed_data.append({
            "item_code": row.get("item_code"),
            "item_name": row.get("item_name"),
            "total_qty": row.get("total_qty"),
            "wo_name": row.get("wo_name"),
            "planned_start_date":row.get("planned_start_date"),
            "available_qty": available_qty,
            "expected_qty": expected_qty,
            "short_qty": short_qty
        })

    return processed_data


def get_available_qty(item_code, warehouse):
    available_qty = frappe.db.sql("""
        SELECT COALESCE(SUM(actual_qty), 0) AS available_qty
        FROM `tabBin`
        WHERE item_code = %s AND warehouse = %s
    """, (item_code, warehouse), as_dict=True)

    return available_qty[0].get("available_qty", 0)

def get_expected_qty(item_code, from_date, to_date):
    expected_qty = frappe.db.sql("""
        SELECT COALESCE(SUM(qty), 0) AS expected_qty
        FROM `tabPurchase Order Item` poi
        LEFT JOIN `tabPurchase Order` po ON po.name = poi.parent
        WHERE poi.item_code = %s
            AND po.docstatus < 2
            AND po.transaction_date BETWEEN %s AND %s
    """, (item_code, from_date, to_date), as_dict=True)

    return expected_qty[0].get("expected_qty", 0)



def get_coloums(filters):
	coloums = [
		{
			"label":_("Item"),
			"fieldname": "item_code",
			"fieldtype": "Link",
			"options":"Item",
			"width": 150
		},
		{
			"label":_("Item Name"),
			"fieldname": "item_name",
			"fieldtype": "Data",
			"width": 150
		},
		{
			"label":_("Total Required Qty"),
			"fieldname": "total_qty",
			"fieldtype": "Float",
			"width": 120
		},
		{
			"label":_("Available Qty"),
			"fieldname": "available_qty",
			"fieldtype": "Float",
			"width": 120
		},
        {
			"label":_("Short Qty"),
			"fieldname": "short_qty",
			"fieldtype": "Float",
			"width": 120
		},
		{
			"label":_("Expecetd Qty"),
			"fieldname": "expected_qty",
			"fieldtype": "Float",
			"width": 120
		},
		{
			"label":_("Planned Start Date"),
			"fieldname": "planned_start_date",
			"fieldtype": "Date",
			"width": 130
		},
		{
			"label":_("Work Order"),
			"fieldname": "wo_name",
			"fieldtype": "Small Text",
			"width": 450
		},
	]

	return coloums