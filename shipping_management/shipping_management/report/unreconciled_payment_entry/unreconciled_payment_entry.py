# Copyright (c) 2013, Raj Tailor and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from erpnext.controllers.accounts_controller import get_advance_payment_entries
from frappe import _
import erpnext


def execute(filters=None):
	columns, data = get_column(filters),get_data(filters)
	return columns, data

def get_data(filters):
	unallocated_payment_entries = frappe.db.sql("""select posting_date,party_type,party, name as payment_entry, unallocated_amount as amount from `tabPayment Entry` where docstatus = 1 and unallocated_amount > 0 and posting_date between %s and %s order by posting_date""",(filters.get("from_date"),filters.get("to_date")), as_dict=1)
	# data = frappe.db.sql("""""",(filters.get("from_date"),filters.get("to_date")))
	jv_entry = frappe.db.sql("""
		select
			"Journal Entry" as reference_type, t1.name as reference_name,
			t1.posting_date, t1.remark as remarks, t2.name as reference_row,
			t2.credit_in_account_currency as amount, t2.is_advance,
			t2.account_currency as currency,t2.party
		from
			`tabJournal Entry` t1, `tabJournal Entry Account` t2
		where
			t1.name = t2.parent and t1.docstatus = 1 and t2.docstatus = 1
			and t2.party_type = "Customer" 
			and t2.account = "Debtors - T" and t2.credit_in_account_currency > 0
			and (t2.reference_type is null or t2.reference_type = '' or 
			(t2.reference_type in ('Sales Order', 'Purchase Order') and
			t2.reference_name is not null and t2.reference_name != ''))""", as_dict=1)
	frappe.msgprint(str(jv_entry))
	return unallocated_payment_entries

def get_jv_entries(self):
	dr_or_cr = ("credit_in_account_currency" if erpnext.get_party_account_type(self.party_type) == 'Receivable'
		else "debit_in_account_currency")

	bank_account_condition = "t2.against_account like %(bank_cash_account)s" \
			if self.bank_cash_account else "1=1"

	limit_cond = "limit %s" % self.limit if self.limit else ""

	journal_entries = frappe.db.sql("""
		select
			"Journal Entry" as reference_type, t1.name as reference_name,
			t1.posting_date, t1.remark as remarks, t2.name as reference_row,
			{dr_or_cr} as amount, t2.is_advance,
			t2.account_currency as currency
		from
			`tabJournal Entry` t1, `tabJournal Entry Account` t2
		where
			t1.name = t2.parent and t1.docstatus = 1 and t2.docstatus = 1
			and t2.party_type = %(party_type)s and t2.party = %(party)s
			and t2.account = %(account)s and {dr_or_cr} > 0
			and (t2.reference_type is null or t2.reference_type = '' or
				(t2.reference_type in ('Sales Order', 'Purchase Order')
					and t2.reference_name is not null and t2.reference_name != ''))
			and (CASE
				WHEN t1.voucher_type in ('Debit Note', 'Credit Note')
				THEN 1=1
				ELSE {bank_account_condition}
			END)
		order by t1.posting_date {limit_cond}
		""".format(**{
			"dr_or_cr": dr_or_cr,
			"bank_account_condition": bank_account_condition,
			"limit_cond": limit_cond
		}), {
			"party_type": self.party_type,
			"party": self.party,
			"account": self.receivable_payable_account,
			"bank_cash_account": "%%%s%%" % self.bank_cash_account
		}, as_dict=1)

	return list(journal_entries)




def get_dr_or_cr_notes(self):
	dr_or_cr = ("credit_in_account_currency"
		if erpnext.get_party_account_type(self.party_type) == 'Receivable' else "debit_in_account_currency")

	reconciled_dr_or_cr =  ("debit_in_account_currency"
		if dr_or_cr == "credit_in_account_currency" else "credit_in_account_currency")

	voucher_type = ('Sales Invoice'
		if self.party_type == 'Customer' else "Purchase Invoice")

	return frappe.db.sql(""" SELECT doc.name as reference_name, %(voucher_type)s as reference_type,
			(sum(gl.{dr_or_cr}) - sum(gl.{reconciled_dr_or_cr})) as amount,
			account_currency as currency,gl.posting_date
		FROM `tab{doc}` doc, `tabGL Entry` gl
		WHERE
			(doc.name = gl.against_voucher or doc.name = gl.voucher_no)
			and doc.{party_type_field} = %(party)s
			and doc.is_return = 1 and ifnull(doc.return_against, "") = ""
			and gl.against_voucher_type = %(voucher_type)s
			and doc.docstatus = 1 and gl.party = %(party)s
			and gl.party_type = %(party_type)s and gl.account = %(account)s
		GROUP BY doc.name
		Having
			amount > 0
	""".format(
		doc=voucher_type,
		dr_or_cr=dr_or_cr,
		reconciled_dr_or_cr=reconciled_dr_or_cr,
		party_type_field=frappe.scrub(self.party_type)),
		{
			'party': self.party,
			'party_type': self.party_type,
			'voucher_type': voucher_type,
			'account': self.receivable_payable_account
		}, as_dict=1)




def get_column(filters):

	return[
		{
			"label": _("Posting Date"),
			"fieldname": "posting_date",
			"fieldtype": "Date",
			"width": 150
		},
		{
			"label": _("Payment Entry"),
			"fieldname": "payment_entry",
			"fieldtype": "Link",
			"options":"Payment Entry",
			"width": 200
		},
		{
			"label": _("Party Type"),
			"fieldname": "party_type",
			"fieldtype": "Data",
			"width": 200
		},
		{
			"label": _("Party"),
			"fieldname": "party",
			"fieldtype": "Data",
			"width": 200
		},
		# 
		{
			"label": _("Amount"),
			"fieldname": "amount",
			"fieldtype": "Float",
			"width": 200
		}

	]
