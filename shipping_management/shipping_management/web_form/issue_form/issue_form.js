frappe.ready(function() {
	// bind events here
	if(frappe.session.user === "Guest"){
		frappe.web_form.set_df_property("custom_issue_raised_by", "reqd", 1)
		frappe.web_form.set_df_property("customer", "hidden", 1)
	}else{
		frappe.web_form.set_df_property("custom_issue_raised_by", "reqd", 0)
		frappe.web_form.set_df_property("custom_issue_raised_by", "hidden", 1)
		frappe.web_form.set_df_property("customer", "hidden", 0)
	}
})
